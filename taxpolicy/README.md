# Command used to create tax policy website archive

wget --warc-file IRD-taxpolicy-CRAWL-202011251130 --warc-cdx --warc-header="operator: Policy and Strategy, Inland Revenue, New Zealand" -mkEpnp --span-hosts --domains taxpolicy.ird.govt.nz --wait 1 -e robots=off https://taxpolicy.ird.govt.nz/
