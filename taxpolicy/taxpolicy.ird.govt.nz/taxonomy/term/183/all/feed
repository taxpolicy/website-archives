<?xml version="1.0" encoding="utf-8" ?><rss version="2.0" xml:base="https://taxpolicy.ird.govt.nz/taxonomy/term/183/all" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>Investment income</title>
    <link>https://taxpolicy.ird.govt.nz/taxonomy/term/183/all</link>
    <description></description>
    <language>en</language>
          <item>
    <title>Many better off under FDR rules - Dunne</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-04-01-many-better-under-fdr-rules-dunne</link>
    <description>&lt;p&gt;Recent media reports have revealed confusion about how individual investors in overseas companies are taxed under the new fair dividend rate (FDR) rules, Revenue Minister Peter Dunne said in a statement released today. For more information see the &lt;a href=&quot;/news/2009-04-01-many-better-under-fdr-rules-dunne#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2009-04-01-many-better-under-fdr-rules-dunne&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 31 Mar 2009 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">600 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Legislation enacted</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-12-19-legislation-enacted</link>
    <description>&lt;p&gt;Legislation introduced in May in the Taxation (Annual Rates, Savings Investment, and Miscellaneous Provisions) Bill has been enacted. It passed through its final stages in Parliament on 12 December and received Royal assent yesterday. The resulting &lt;a href=&quot;/publications/2006-act-simp/overview&quot;&gt;Taxation (Savings Investment and Miscellaneous Provisions) Act 2006&lt;/a&gt; and the &lt;a href=&quot;/publications/2006-act-arit/overview&quot;&gt;Taxation (Annual Rates of Income Tax 2006-07) Act 2006&lt;/a&gt; are published here, courtesy of Legislation Direct.&lt;/p&gt;
</description>
     <pubDate>Mon, 18 Dec 2006 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">457 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Parliament passes tax bills</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-12-13-parliament-passes-tax-bills</link>
    <description>&lt;p&gt;Parliament last night passed the Taxation (Savings Investment and Miscellaneous Provisions) Bill and the Taxation (Annual Rates of Income Tax) Bill, introduced in May as a single bill. The main focus of the new legislation is reform of the tax rules on income from share investments made through managed funds and by individuals. Other measures include changes relating to &amp;quot;salary sacrifice&amp;quot;, people in New Zealand who have interests in Australian superannuation schemes, and extension of the KiwiSaver SSCWT tax exemption. The new legislation is expected to receive Royal assent within a few days. For more information see the &lt;a href=&quot;/news/2006-12-13-parliament-passes-tax-bills#statement&quot;&gt;media statement&lt;/a&gt; from the Minister of Revenue.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-12-13-parliament-passes-tax-bills&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 12 Dec 2006 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">455 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Tax bill reported back</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-12-06-tax-bill-reported-back</link>
    <description>&lt;p&gt;The Finance and Expenditure Committee has reported to Parliament on the Taxation (Annual Rates, Savings Investment, and Miscellaneous Provisions) Bill, introduced in May 2006. The central feature of the bill is a comprehensive reform of the taxation of income from investment through managed funds and from offshore portfolio investment in shares. The main change recommended by the committee is to replace the originally proposed method of taxing income from offshore share investments with a &amp;quot;fair dividend rate&amp;quot; method. For information on all the changes recommended by the committee see the &lt;a href=&quot;/publications/2006-bill-arsimp-reported-back/overview&quot;&gt;bill as reported back&lt;/a&gt; and the four-volume &lt;a href=&quot;/publications/2006-or-arsimp/overview&quot;&gt;officials&#039; report on submissions&lt;/a&gt;.&lt;/p&gt;
</description>
     <pubDate>Tue, 05 Dec 2006 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">451 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Change to offshore tax proposals</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-09-15-change-offshore-tax-proposals</link>
    <description>&lt;p&gt;The government has proposed a &amp;quot;fair dividend rate&amp;quot; approach to calculating tax on income from offshore shares, to replace the method proposed in the taxation bill currently before Parliament. It would tax individual investors on a maximum of 5% of the market value of their offshore shares at the beginning of the year, with returns of less than 5% attracting proportionately less tax. No tax would be payable when losses are made. For more information see the &lt;a href=&quot;/news/2006-09-15-change-offshore-tax-proposals#statement&quot;&gt;media statement&lt;/a&gt; and &lt;a href=&quot;/news/2006-09-15-change-offshore-tax-proposals#qanda&quot;&gt;questions and answers&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-09-15-change-offshore-tax-proposals&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Thu, 14 Sep 2006 13:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">431 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Speech: tax policy update</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-08-24-speech-tax-policy-update</link>
    <description>&lt;p&gt;In a speech today to the Ernst Young Tax Conference, Revenue Minister Peter Dunne discussed the Business Tax Review, the proposed reform of the tax treatment of income from share investment, the life insurance tax review, and the forthcoming discussion document on New Zealand&#039;s international tax rules. For more information see the Minister&#039;s &lt;a href=&quot;/news/2006-08-24-speech-tax-policy-update#statement&quot;&gt;speech&lt;/a&gt;.&lt;/p&gt;
&lt;hr /&gt;
&lt;p&gt;&lt;a name=&quot;statement&quot;&gt;&lt;/a&gt;Hon Peter Dunne&lt;br /&gt;
Minister of Revenue&lt;/p&gt;
&lt;p&gt;SPEECH&lt;/p&gt;
&lt;h3&gt;Ernst and Young Tax Conference&lt;/h3&gt;
&lt;h3&gt;Hotel Inter-Continental, Wellington&lt;/h3&gt;
&lt;p&gt;Thank you for the invitation to speak to you today on the Business Tax Review and other tax policy matters that I think should be raised.&lt;/p&gt;
&lt;p&gt;The confidence and supply agreement signed between United Future and Labour resulted in the addition of three important items to the government&#039;s tax policy work programme. They were:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;the preparation and release of a government discussion paper on the merits of income splitting for families;&lt;/li&gt;
&lt;li&gt;introduction of a new regime for the tax treatment of charities and charitable donations; and&lt;/li&gt;
&lt;li&gt;the Business Tax Review.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Chief of these was the Business Tax Review. The stated purpose of the Review was to ensure the tax system works to give better incentives for productivity gains and improved competitiveness with Australia.&lt;/p&gt;
&lt;p&gt;In the course of the post-election negotiations, we laid particular emphasis on business tax reform because of what we regarded as the more pressing need &amp;ndash; especially since the advent of Working for Families. That package has enhanced the financial situation of nearly 350,000 New Zealand families.&lt;/p&gt;
&lt;p&gt;The discussion document was released a month ago, the culmination of several months&#039; work in considering a wide range of possibilities, including many practices from other countries, for changes to the New Zealand tax system.&lt;/p&gt;
&lt;p&gt;It describes a range of possible business tax initiatives that will help to increase productivity and boost New Zealand&#039;s international competitiveness. It also seeks the public&#039;s views on other ideas that should be considered.&lt;/p&gt;
&lt;p&gt;New Zealand&#039;s wage rates currently lag behind those in many OECD countries including Australia. The only way that New Zealand can obtain a sustained growth in wage rates is for labour to become more productive. Greater labour productivity means that the same amount of labour will provide more GDP.&lt;/p&gt;
&lt;p&gt;Labour productivity can be increased in a number of ways. A forestry worker with an axe is less productive than a worker with a chainsaw. Increased investment in plant, equipment and buildings can boost productivity.&lt;/p&gt;
&lt;p&gt;Similarly, a better-educated workforce or more innovative technologies will boost labour productivity. A critical issue for the government is whether there are ways that New Zealand&#039;s tax system impedes growth in labour productivity.&lt;/p&gt;
&lt;p&gt;It is also important that New Zealand&#039;s capital ends up going to those firms which will use it to generate the best returns for New Zealand as a whole.&lt;/p&gt;
&lt;p&gt;Other things being equal, a broad income base and low tax rates encourage capital being acquired by firms that will use it most productively.&lt;/p&gt;
&lt;p&gt;If, however, firms cannot capture all of the benefits of their investment, they may undertake too little investment from the point of view of New Zealand as a whole. In this case, there may be grounds for providing tax incentives to encourage more productive investment from the point of view of New Zealand as a whole.&lt;/p&gt;
&lt;p&gt;Our tax system also needs to be internationally competitive, especially in relation to Australia&#039;s. There are many ways in which our tax system is better for business than the Australian tax system is. For example, New Zealand does not have Australia&#039;s general capital gains tax, payroll taxes or stamp duties. However, we currently have a higher statutory company rate of 33 percent compared with Australia&#039;s rate of 30 percent.&lt;/p&gt;
&lt;p&gt;In a fundamental review of business taxation, there are many different changes that could conceivably boost productivity and competitiveness.&lt;/p&gt;
&lt;p&gt;One option that was considered but rejected was that of deep company rate cuts. That option has a number of potential problems that are described in more detail in the discussion document.&lt;/p&gt;
&lt;p&gt;Deep company tax rate cuts have attractions as a means of enhancing productivity and competitiveness. However, they can also be very expensive and, if implemented, would require a major replacement source of revenue if New Zealand is to be able to continue to maintain its current level of government services.&lt;/p&gt;
&lt;p&gt;Deep company tax rate cuts would also widen the gap between personal and company tax rates, which would increase the benefits of people using companies to shelter income from higher personal tax rates.&lt;/p&gt;
&lt;p&gt;The fact that New Zealand has a full imputation system means that deep company rate cuts would provide a permanent benefit to non-resident and non-taxpayer shareholders, but only a timing benefit to domestic taxpayers. They would also be of benefit only to businesses operating as companies.&lt;/p&gt;
&lt;p&gt;There was also the question of a replacement source of revenue. We considered a payroll tax as a possible candidate. In principle, a payroll tax on all employers as well as the income of the self-employed may be a relatively neutral form of tax for funding deep company rate cuts.&lt;/p&gt;
&lt;p&gt;It would be costly, however, for smaller firms to comply with such a tax. Excluding the self-employed and providing thresholds for smaller firms would reduce compliance costs. At the same time, it would require a higher rate on taxpaying firms, which would bias labour into non-taxed firms. This would not promote labour productivity. In the end, we decided that this was an unattractive direction for us to pursue.&lt;/p&gt;
&lt;p&gt;These, then, are some the considerations that have led to the range of possible options outlined in the discussion document.&lt;/p&gt;
&lt;p&gt;Obviously, tax policy by itself cannot build a high wage, high skill, knowledge-based economy. But what it can do is to remove tax obstacles to achieving that, and ensure that our business tax rules support innovation, business investment and the development of a highly skilled workforce, and that they encourage exporters to break into new markets.&lt;/p&gt;
&lt;p&gt;The discussion document suggests a range of tax options for doing just that.&lt;/p&gt;
&lt;p&gt;It also invites further suggestions on other options that meet the objectives of the Review. It would not be possible to progress the full range of options, so informed trade-offs will need to be made. One of the aims for the discussion document is to generate an informed debate and feedback on the relative merits of the different options.&lt;/p&gt;
&lt;p&gt;The first option described in the discussion document is reducing the company tax rate from 33 percent to 30 percent, which would align it with Australia&#039;s corporate tax rate. That would have an estimated cost of $540 million a year.&lt;/p&gt;
&lt;p&gt;Reducing the company tax rate to 30 percent would raise our productivity and growth in a number of ways.&lt;/p&gt;
&lt;p&gt;It would boost the competitiveness of New Zealand-based companies, and encourage inbound investment by firms that had decided to move to New Zealand. In turn, that would tend to increase our stock of plant, equipment and building, which would boost labour productivity and wage rates.&lt;/p&gt;
&lt;p&gt;On the other hand, there are disadvantages to reducing the company tax rate by itself, and they have to be weighed against the benefits of doing so.&lt;/p&gt;
&lt;p&gt;The disadvantages include the problems arising from widening the gap between the company tax rate and the top personal tax rate, and between the company tax rate and the 33 percent tax rate for trusts.&lt;/p&gt;
&lt;p&gt;As the discussion document points out, the current gap provides incentives for companies and trusts to be used to shelter income from higher personal tax rates. Therefore reducing it would increase those incentives.&lt;/p&gt;
&lt;p&gt;Clearly, reducing the company tax rate has implications for personal taxation, which is acknowledged in the discussion document.&lt;/p&gt;
&lt;p&gt;We are moving one step at a time, however. The emphasis in the Review is on business tax because this is the most pressing need, and because business tax reforms are more complex and therefore take more time to design and develop.&lt;/p&gt;
&lt;p&gt;Once we have made decisions on the matters raised in the Review, we will look at the wider implications in other areas of the tax system. Decisions on personal taxation will be made within the fiscal confines at that time.&lt;/p&gt;
&lt;p&gt;The discussion document also suggests a number of tax base initiatives that would improve productivity, business investment and competitiveness.&lt;/p&gt;
&lt;p&gt;It raises the possibility of introducing targeted tax credits for R &amp;amp; D activities, export market development and skills improvement in the workforce. When businesses invest in these areas there are wider benefits to the country as a whole.&lt;/p&gt;
&lt;p&gt;The government already provides support for this type of investment by means of various grant programmes, and thinks it worth considering the provision of further support by way of tax concessions.&lt;/p&gt;
&lt;p&gt;Businesses invest in R &amp;amp; D to improve their products and processes, and that contributes to productivity and competitiveness. At present, many businesses under-invest in R &amp;amp; D because they do not capture all the benefits. R &amp;amp; D tax credits should help to resolve the problem of under-investment in this area.&lt;/p&gt;
&lt;p&gt;Tax credits for export market development are another possibility. They could be a solution to businesses under-investing in developing new markets, which has wider implications for other businesses and for the economy as a whole.&lt;/p&gt;
&lt;p&gt;Similarly, raising skill levels should also help to increase productivity. Employers may be reluctant, at present, to spend more on developing the skills of their employees because they are easily lost to the business when employees change jobs, though the skills are not necessarily lost to New Zealand.&lt;/p&gt;
&lt;p&gt;Tax credits for skills enhancement should help to reduce under-investment in skills development as well.&lt;/p&gt;
&lt;p&gt;Also on the list of possibilities are adjustments to the depreciation tax rules. Accelerated depreciation would reduce the cost of investing in assets, which would encourage more businesses to upgrade their plant and equipment. Using more efficient plant and equipment can make labour more productive and so increase economic output.&lt;/p&gt;
&lt;p&gt;The discussion document also outlines further compliance cost reduction measures aimed at raising productivity and improving competitiveness. The rationale is that the less time and money businesses have to spend on complying with their tax requirements the more they have to expand and invest.&lt;/p&gt;
&lt;p&gt;The release of the discussion document is the first step in engaging with business and with the public on how the tax system can contribute most effectively to improving productivity and our ability to compete internationally.&lt;/p&gt;
&lt;p&gt;It seeks your views on the possible options, asking you to rank them in order of preference, and welcomes your alternative suggestions. I urge you to take part in the process.&lt;/p&gt;
&lt;p&gt;Submissions close on 8 September and &amp;ndash; owing to time constraints &amp;ndash; any submissions received after the closing date are unlikely to be considered. I&#039;m told that several submissions have already been received.&lt;/p&gt;
&lt;p&gt;Once we have received and analysed your feedback, we will decide on the matters raised in the review and in the feedback, probably early next year. I anticipate introducing legislation in May 2007, so that resulting changes can come into force on 1 April 2008.&lt;/p&gt;
&lt;p&gt;To turn briefly to other matters, the taxation bill currently before Parliament has attracted over 3000 submissions, I&#039;m told, which must be a record for a taxation bill.&lt;/p&gt;
&lt;p&gt;Many of the submissions relate to the proposed reform of the tax treatment of income from different types of share investments.&lt;/p&gt;
&lt;p&gt;The current rules contain many anomalies that must be addressed &amp;ndash; for example, taxing income earned in some countries more favourably than in other countries, and taxing income earned by individuals more favourably than income earned through managed funds.&lt;/p&gt;
&lt;p&gt;Obviously, the most controversial aspect of the Bill is the proposed change to the tax treatment of investments into grey-list countries in particular, which has raised the spectre of an effective capital gains tax on unrealised gains.&lt;/p&gt;
&lt;p&gt;The select committee will undoubtedly focus a fair amount of its attention on this issue, and whether there are other ways to address the anomalies I have referred to, bearing in mind that both United Future and the government have ruled out moving to a comprehensive capital gains tax.&lt;/p&gt;
&lt;p&gt;One-half of the reform, the portfolio investment entity rules, removes the disadvantages facing people who invest through managed funds, which is especially pertinent to KiwiSaver.&lt;/p&gt;
&lt;p&gt;As you may know, the Finance and Expenditure Committee has reported back to Parliament on the KiwiSaver Bill, recommending that the start-up date of the scheme be delayed for three months.&lt;/p&gt;
&lt;p&gt;It is therefore desirable to delay the implementation date of the portfolio entity rules, to fit in with the timing of KiwiSaver, since the two are closely linked. I&#039;ll be making an announcement to this effect shortly.&lt;/p&gt;
&lt;p&gt;The Finance and Expenditure Committee will be hearing oral submissions on the bill over the next few weeks.&lt;/p&gt;
&lt;p&gt;I think it likely that the select committee will recommend some changes to the proposed legislation because, in my experience, taxation bills nearly always change as a result of select committee scrutiny. I await the committee&#039;s report with interest.&lt;/p&gt;
&lt;p&gt;As the Minister of Finance and I announced last week, the government is to carry out a comprehensive review of the life insurance tax rules.&lt;/p&gt;
&lt;p&gt;The review has been welcomed by the life insurance industry, which over the years has raised a number of anomalies in the tax rules. The rules date back to 1990, though in the ensuing sixteen years there have been a number of changes in the commercial, regulatory and saving environment &amp;ndash; which probably means that the rules need to be updated.&lt;/p&gt;
&lt;p&gt;Although the scope of the review is to be established in consultation with the industry, it is certain to consider the question of whether life insurance should be included within the scope of the proposed rules for taxing portfolio investment entities.&lt;/p&gt;
&lt;p&gt;Not to include life insurance would be to disadvantage it relative to other savings vehicles. And life insurance constitutes a major sector of the savings industry, with life funds under management totalling about $9 billion. To put that figure into perspective, unit trust assets total about $18 billion, superannuation assets nearly $20 billion, and those of other funds close to $14 billion.&lt;/p&gt;
&lt;p&gt;The aim of the review is to create a robust and flexible environment that is favourable to all &amp;ndash; policyholders, life insurers and government alike. It is not possible at this stage to determine the fiscal impact of the review, though integrating life insurance within the portfolio investment entity rules would likely reduce the amount of tax being raised from life insurance in the short term.&lt;/p&gt;
&lt;p&gt;Life insurance is a technically complex area and so, for purposes of consultation, policy officials will be releasing a series of targeted, technical issues papers for comment by the industry and stakeholders.&lt;/p&gt;
&lt;p&gt;As many of the issues to be discussed have already been raised by the industry, it is reasonable to expect a vigorous and constructive response.&lt;/p&gt;
&lt;p&gt;The Business Tax Review discussion document mentioned our intention to release a discussion document on international tax by the end of this year.&lt;/p&gt;
&lt;p&gt;The re examination of our international tax rules will be based on several objectives familiar from the Business Tax Review. They are:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;improving the productivity and competitiveness of New Zealand businesses;&lt;/li&gt;
&lt;li&gt;progressing economic transformation, which includes promoting a vibrant export sector and globally competitive businesses;&lt;/li&gt;
&lt;li&gt;upholding the tax policy principles of fairness, efficiency, simplicity and integrity; and&lt;/li&gt;
&lt;li&gt;ensuring consistency between tax policy and other government initiatives.&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;In practice, that means the main focus of the review is the taxation of outbound, non-portfolio investment. In particular, the government is committed to taking a fresh look at whether we should continue with our current controlled foreign company rules.&lt;/p&gt;
&lt;p&gt;The review will canvass options for changing those rules, as well as any associated changes to the conduit and thin capitalisation rules that might stem from the adoption of those options for reform.&lt;/p&gt;
&lt;p&gt;As far as inbound investment is concerned, the government plans to look at whether any adjustments should be made to New Zealand&#039;s longstanding treaty policy on non-resident withholding tax rates on dividends, interest and royalties.&lt;/p&gt;
&lt;p&gt;This is a question that needs to be resolved before any renegotiation of the Australia-New Zealand double tax agreement occurs.&lt;/p&gt;
&lt;p&gt;It is important to note, however, and this is an important point, that decisions to reduce treaty rates are critical ones because, once implemented through a treaty, they cannot be increased by any government unilaterally without terminating or renegotiating that treaty.&lt;/p&gt;
&lt;p&gt;So, including the Business Tax Review, changes to the investment rules and the international tax review, there is a very full tax reform agenda.&lt;/p&gt;
&lt;p&gt;I believe that a critical part of good tax reform in New Zealand is the open consultation process, and I value your contribution to these issues.&lt;/p&gt;
&lt;p&gt;I wish you the very best for the remainder of your conference.&lt;/p&gt;
</description>
     <pubDate>Wed, 23 Aug 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">420 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Measures added to tax bill</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-06-21-measures-added-tax-bill</link>
    <description>&lt;p&gt;Supplementary Order Papers released today have added further measures to the taxation bill currently before Parliament: a technical change to the GST grouping rules, and the previously announced five-year exemption from the proposed tax rules for taxing offshore share income. The proposed application date of an amendment in the bill relating to consolidated groups has also been changed. For more information see the &lt;a href=&quot;/news/2006-06-21-measures-added-tax-bill#statement&quot;&gt;media statement&lt;/a&gt;, &lt;a href=&quot;/publications/2006-sop-45-arsimp/overview&quot;&gt;Supplementary Order Paper No. 45&lt;/a&gt; for the GST amendment and &lt;a href=&quot;/publications/2006-sop-44-arsimp/overview&quot;&gt;Supplementary Order Paper No. 44&lt;/a&gt; for the others.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-06-21-measures-added-tax-bill&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 20 Jun 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">407 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Cullen on superannuation, investment</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-06-07-cullen-superannuation-investment</link>
    <description>&lt;p&gt;Finance Minister Michael Cullen outlined the government&#039;s superannuation and investment strategy in a speech today to the Superannuation and Investment Conference in Wellington. He also discussed tax-related initiatives, including proposed changes to the taxation of income from direct portfolio investment in overseas shares. For more information see the Minister&#039;s &lt;a href=&quot;/news/2006-06-07-cullen-superannuation-investment#speech&quot;&gt;speech&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-06-07-cullen-superannuation-investment&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 06 Jun 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">406 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Closing date for bill submissions</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-05-26-closing-date-bill-submissions</link>
    <description>&lt;p&gt;The closing date for submissions on the &lt;a href=&quot;/publications/2006-bill-arsimp/overview&quot;&gt;Taxation (Annual Rates, Savings Investment, and Miscellaneous Provisions) Bill&lt;/a&gt; is 7 July. The bill was introduced on 17 May, had its &lt;a href=&quot;https://www.beehive.govt.nz/node/25923&quot;&gt;first reading on 25 May&lt;/a&gt; and was referred to Parliament&amp;#39;s Finance and Expenditure Committee for consideration. Information on how to make a submission is available at &lt;a href=&quot;http://www.parliament.nz/en-nz/about-parliament/get-involved/submission/00CLOOCHvYrSaySubmission1/how-to-make-a-submission&quot;&gt;www.clerk.parliament.govt.nz&lt;/a&gt;.&lt;/p&gt;
</description>
     <pubDate>Thu, 25 May 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">402 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Bill reforms taxation of investment income</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-05-17-bill-reforms-taxation-investment-income</link>
    <description>&lt;p&gt;A bill tabled in Parliament today introduces a wide-ranging reform of the rules on taxing income from investment through New Zealand collective investment vehicles and from offshore portfolio investment in shares. Other matters in the bill include changes to minimise the use of excessive salary sacrifice as a means of paying less tax, and to resolve compliance problems for people who have interests in Australian superannuation schemes.&lt;/p&gt;
&lt;p&gt;For information on these and other matters in the bill see the government&#039;s &lt;a href=&quot;/news/2006-05-17-bill-reforms-taxation-investment-income#statement&quot;&gt;media statement&lt;/a&gt;, &lt;a href=&quot;/news/2006-05-17-bill-reforms-taxation-investment-income#qanda&quot;&gt;questions and answers about the investment proposals&lt;/a&gt;, and the &lt;a href=&quot;/publications/2006-commentary-arsimp/overview&quot;&gt;commentary&lt;/a&gt; on the bill. The &lt;a href=&quot;/publications/2006-bill-arsimp/overview&quot;&gt;Taxation (Annual Rates, Savings Investment, and Miscellaneous Provisions) Bill&lt;/a&gt; is published here, courtesy of Legislation Direct.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-05-17-bill-reforms-taxation-investment-income&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 16 May 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">398 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Holiday from new tax rules announced</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-05-17-holiday-new-tax-rules-announced</link>
    <description>&lt;p&gt;Investors in widely held foreign companies that have a substantial New Zealand shareholder base, such as Guinness Peat Group, will be granted a five-year holiday from the proposed rules for taxing offshore share investment, the Minister of Revenue announced today. It will be limited to investment in companies that meet certain criteria. The change will be made by means of Supplementary Order Paper to the taxation bill introduced today. For more information see the &lt;a href=&quot;/news/2006-05-17-holiday-new-tax-rules-announced#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-05-17-holiday-new-tax-rules-announced&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 16 May 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">400 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>New rules benefit funds, savers</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-05-16-new-rules-benefit-funds-savers</link>
    <description>&lt;p&gt;Managed funds and their investors will be better off under the proposed new rules on taxing income from share investment, the Minister of Revenue said today. In response to claims that the changes would disadvantage funds and savers through funds, Mr Dunne cited statements from the funds industry that supported the changes. The bill introducing the new rules is expected to be tabled in Parliament this week. For more information see the Minister&#039;s &lt;a href=&quot;/news/2006-05-16-new-rules-benefit-funds-savers#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-05-16-new-rules-benefit-funds-savers&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Mon, 15 May 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">399 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Investment tax changes announced</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-04-11-investment-tax-changes-announced</link>
    <description>&lt;p&gt;The government today announced details of its proposed reform of the taxation of investment income. The changes are designed to remove inconsistencies in the current rules, which over-tax investors using New Zealand-based managed funds, advantage direct investors in overseas shares, and favour investment in certain countries over others. For more information see:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;The government&#039;s &lt;a href=&quot;/news/2006-04-11-investment-tax-changes-announced#statement&quot;&gt;media statement&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;/sites/default/files/news/2006-04-11-investment-fact-sheet-1.pdf&quot;&gt;Fact sheet 1: Savers in New Zealand managed funds&lt;/a&gt; (PDF 96KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;/sites/default/files/news/2006-04-11-investment-fact-sheet-2.pdf&quot;&gt;Fact sheet 2: Overseas share investments by individual direct investors&lt;/a&gt; (PDF 113KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;/sites/default/files/news/2006-04-11-investment-fact-sheet-3.pdf&quot;&gt;Fact sheet 3: New tax rules for New Zealand managed funds&lt;/a&gt; (PDF 83KB)&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;/sites/default/files/news/2006-04-11-investment-qanda.pdf&quot;&gt;Questions and answers&lt;/a&gt; (PDF 77KB)&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-04-11-investment-tax-changes-announced&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Mon, 10 Apr 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">395 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Investment tax changes in May bill</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-03-29-investment-tax-changes-may-bill</link>
    <description>&lt;p&gt;
	A tax bill scheduled for introduction in May is expected to introduce changes relating domestic investment through collective investment vehicles and to offshore portfolio investment in shares, Revenue Minister Peter Dunne said today. The government will announce details of the proposed changes over the next few weeks, the Minister told the SuperFunds Summit in Wellington today. For more information see the Minister&amp;#39;s &lt;a href=&quot;/news/2006-03-29-investment-tax-changes-may-bill#speech&quot;&gt;speech&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-03-29-investment-tax-changes-may-bill&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 28 Mar 2006 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">389 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Minister on tax reform</title>
    <link>https://taxpolicy.ird.govt.nz/news/2006-02-08-minister-tax-reform</link>
    <description>&lt;p&gt;In a speech today to the Financial Planners and Insurance Advisors Association, Revenue Minister Peter Dunne discussed the government&amp;rsquo;s business tax review and progress in policy areas such as reform of the tax rules on investment income. For more information see the minister&#039;s &lt;a href=&quot;/news/2006-02-08-minister-tax-reform#speech&quot;&gt;speech&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2006-02-08-minister-tax-reform&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 07 Feb 2006 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">376 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  </channel>
</rss>