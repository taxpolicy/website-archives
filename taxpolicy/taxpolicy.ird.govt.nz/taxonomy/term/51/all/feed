<?xml version="1.0" encoding="utf-8" ?><rss version="2.0" xml:base="https://taxpolicy.ird.govt.nz/taxonomy/term/51/all" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <channel>
    <title>PIEs</title>
    <link>https://taxpolicy.ird.govt.nz/taxonomy/term/51/all</link>
    <description></description>
    <language>en</language>
          <item>
    <title>Issues paper released on taxation of foreign funds in PIEs</title>
    <link>https://taxpolicy.ird.govt.nz/news/2010-04-14-issues-paper-released-taxation-foreign-funds-pies</link>
    <description>&lt;p&gt;
	An issues paper released today seeks views on two possible options for exempting non-residents investing in PIEs. Submissions close on 4 June 2010. For more information see the &lt;a href=&quot;/news/2010-04-14-issues-paper-released-taxation-foreign-funds-pies#statement&quot;&gt;media statement&lt;/a&gt; and the issues paper, &lt;a href=&quot;/publications/2010-ip-non-residents-pies/overview&quot;&gt;Allowing a zero percent tax rate for non-residents investing in a PIE&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2010-04-14-issues-paper-released-taxation-foreign-funds-pies&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Wed, 14 Apr 2010 00:14:00 +0000</pubDate>
 <dc:creator>joannescott</dc:creator>
 <guid isPermaLink="false">1387 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Law change for PIE timing rules</title>
    <link>https://taxpolicy.ird.govt.nz/news/2010-03-31-law-change-pie-timing-rules</link>
    <description>&lt;p&gt;
	The Government is to introduce legislation to clarify the timing rules for portfolio investment entities claiming a tax deduction for credit impairments or doubtful debts. For more information see the &lt;a href=&quot;/news/2010-03-31-law-change-pie-timing-rules#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2010-03-31-law-change-pie-timing-rules&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Wed, 31 Mar 2010 01:49:10 +0000</pubDate>
 <dc:creator>davidnind</dc:creator>
 <guid isPermaLink="false">1385 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Legislation in the July 09 tax bill enacted</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-12-09-legislation-july-09-tax-bill-enacted</link>
    <description>&lt;p&gt;Legislation introduced in the Taxation (Consequential Rate Alignment and Remedial Matters) Bill received Royal assent on 7 December. The bill was introduced in July 2009 and passed through its final stages in Parliament late last month. The resulting Act is published &lt;a href=&quot;/publications/2009-act-crarm/overview&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;Coverage of the new legislation will appear in a &lt;em&gt;Tax Information Bulletin&lt;/em&gt; to be published early next year.&lt;/p&gt;
</description>
     <pubDate>Tue, 08 Dec 2009 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">664 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Rate alignment bill passes</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-11-27-rate-alignment-bill-passes</link>
    <description>&lt;p&gt;The Taxation (Consequential Rate Alignment and Remedial Matters) Bill, introduced in July, passed its final stages in Parliament late yesterday. The main feature of the omnibus bill is the alignment of RWT rates on interest with recent changes to personal tax rates and the company tax rate, and alignment of PIE tax rates with personal tax rates. The new legislation will be available here once it has received Royal assent, which is expected within a few days. For more information see the &lt;a href=&quot;/news/2009-11-27-rate-alignment-bill-passes#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2009-11-27-rate-alignment-bill-passes&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Thu, 26 Nov 2009 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">661 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Tax bill reported back to Parliament</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-11-09-tax-bill-reported-back-parliament</link>
    <description>&lt;p&gt;The Finance and Expenditure Committee has reported back to Parliament on the Taxation (Consequential Rate Alignment and Remedial Matters) Bill, which was introduced in July. The main feature of the bill is the alignment of resident withholding tax rates on interest and portfolio investment entity tax rates with personal and company income tax rates.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2009-11-09-tax-bill-reported-back-parliament&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Sun, 08 Nov 2009 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">652 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>RWT focus of new tax bill</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-07-21-rwt-focus-new-tax-bill</link>
    <description>&lt;p&gt;New resident withholding tax rates on interest paid to individuals, to align them with personal income tax rates, are the central feature of an omnibus taxation bill tabled in Parliament today. The bill also introduces a new default rate and a new 30% rate on interest paid to companies, and aligns the tax rates on PIEs with personal tax rates. For information on these and other changes proposed in the bill, see the &lt;a href=&quot;/news/2009-07-21-rwt-focus-new-tax-bill#statement&quot;&gt;media statement&lt;/a&gt; and the &lt;a href=&quot;/publications/2009-commentary-crarm/overview&quot;&gt;commentary&lt;/a&gt; on the &lt;a href=&quot;/publications/2009-bill-crarm/overview&quot;&gt;Taxation (Consequential Rate Alignment and Remedial Matters) Bill&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2009-07-21-rwt-focus-new-tax-bill&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Mon, 20 Jul 2009 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">620 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Investment &amp; income tax rates to be aligned</title>
    <link>https://taxpolicy.ird.govt.nz/news/2009-07-07-investment-income-tax-rates-be-aligned</link>
    <description>&lt;p&gt;The government announced today that it will introduce legislation to align RWT rates on interest and PIE tax rates with recent changes to personal income tax rates and the 30% company tax rate. The changes will be part of a taxation bill to be introduced later this month, with passage expected by December. For more information see the &lt;a href=&quot;/news/2009-07-07-investment-income-tax-rates-be-aligned#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2009-07-07-investment-income-tax-rates-be-aligned&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Mon, 06 Jul 2009 13:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">617 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Finance Minister&#039;s policy update</title>
    <link>https://taxpolicy.ird.govt.nz/news/2008-07-02-finance-ministers-policy-update</link>
    <description>&lt;p&gt;Finance Minister Michael Cullen&amp;#39;s update to the Deloitte Tax Seminar today included a description of the follow-up work on RWT and PIE tax rates that is being done as a result of the personal tax cuts announced in Budget 2008. He also described business tax reforms contained in the bill introduced today and the process set up to deal with unintended law changes that result from the recent rewrite of the Income Tax Act. For more information see the &lt;a href=&quot;https://www.beehive.govt.nz/node/33848&quot;&gt;speech&lt;/a&gt;.&lt;/p&gt;
</description>
     <pubDate>Tue, 01 Jul 2008 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">558 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Budget tax legislation enacted</title>
    <link>https://taxpolicy.ird.govt.nz/news/2008-05-30-budget-tax-legislation-enacted</link>
    <description>&lt;p&gt;Legislation introduced on 22 May in the Taxation (Personal Tax Cuts, Annual Rates, and Remedial Matters) Bill has been enacted. It passed through its final stages in Parliament on 23 May and received Royal assent on 29 May 2008. The resulting &lt;a href=&quot;/publications/2008-act-ptcarrm/overview&quot;&gt;Act &lt;/a&gt;is published here, courtesy of Legislation Direct.&lt;/p&gt;
</description>
     <pubDate>Thu, 29 May 2008 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">551 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Parliament passes Budget tax bill</title>
    <link>https://taxpolicy.ird.govt.nz/news/2008-05-23-parliament-passes-budget-tax-bill</link>
    <description>&lt;p&gt;The bill introducing tax changes announced in Budget 2008 passed its final stages in Parliament today. The Taxation (Personal Tax Cuts, Annual Rates, and Remedial Matters) Bill, introduced under urgency on 22 May, gives effect to personal tax cuts that are to be phased in over three and a half years and makes complementary changes to Working for Families tax credits. Remedial amendments include changes to give tax-certainty to a variety of organisations in the lead-up to the 1 July deadline for registration with the Charities Commission.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2008-05-23-parliament-passes-budget-tax-bill&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Thu, 22 May 2008 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">549 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Budget 2008 tax announcements</title>
    <link>https://taxpolicy.ird.govt.nz/news/2008-05-22-budget-2008-tax-announcements</link>
    <description>&lt;p&gt;Personal tax cuts to take effect from 1 October this year were announced in Budget 2008. The bottom personal tax rate will be reduced from 15% to 12.5%, and the thresholds at which personal rates apply are to be raised over a period of three and a half years. Complementary changes to Working for Families tax credits will increase the amount of entitlement to the family tax credit and raise the income threshold at which abatement starts. See the &lt;a href=&quot;/sites/default/files/news/2008-05-22-media-statement-tax-cuts-budget-2008.pdf&quot;&gt;media statement&lt;/a&gt; (PDF 226KB) - &amp;quot;Tax cuts for all workers on 1 October&amp;quot; and &lt;a href=&quot;/sites/default/files/news/2008-05-22-fact-sheet-tax-relief-budget-2008.pdf&quot;&gt;Fact sheet on tax relief for individuals and families&lt;/a&gt; (PDF 119KB).&lt;/p&gt;
&lt;p&gt;It was also announced that a bill to be introduced in June will reduce tax-related compliance costs for businesses by raising a number of tax thresholds and remove tax impediments to the offshore expansion of New Zealand-resident businesses. See &lt;a href=&quot;/sites/default/files/news/2008-05-22-joint-statement-budget-2008.pdf&quot;&gt;Tax changes to lower costs for NZ businesses&lt;/a&gt; (PDF 495KB).&lt;/p&gt;
&lt;p&gt;The complete set of fact sheets and questions and answers on the Budget 2008 Personal Tax Package is published at &lt;a href=&quot;http://www.treasury.govt.nz/budget/2008/tax&quot;&gt;www.treasury.govt.nz/budget/2008/tax&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;All ministerial statements contained in the Budget media kit are published at &lt;a href=&quot;https://www.beehive.govt.nz/feature/budget-2008&quot;&gt;www.beehive.govt.nz/budget2008&lt;/a&gt;. Core Budget documents are published at &lt;a href=&quot;http://www.treasury.govt.nz/budget/2008&quot;&gt;www.treasury.govt.nz/budget/2008&lt;/a&gt;.&lt;/p&gt;
</description>
     <pubDate>Wed, 21 May 2008 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">547 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Tax bill introduced</title>
    <link>https://taxpolicy.ird.govt.nz/news/2008-05-22-tax-bill-introduced</link>
    <description>&lt;p&gt;A taxation bill tabled in Parliament this afternoon introduces tax measures announced in Budget 2008: personal tax cuts that are to be phased in over three and a half years, and changes to Working for Families tax credits that take account of inflation. The bill contains a number of remedial amendments that require early enactment.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2008-05-22-tax-bill-introduced&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Wed, 21 May 2008 12:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">548 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Tax legislation enacted</title>
    <link>https://taxpolicy.ird.govt.nz/news/2007-12-20-tax-legislation-enacted</link>
    <description>&lt;p&gt;Legislation introduced in May in the Taxation (Annual Rates, Business Taxation, KiwiSaver, and Remedial Matters) Bill has been enacted. It passed through it final stages in Parliament on 12 December and received Royal assent yesterday afternoon. The resulting acts are published here, courtesy of Legislation Direct:&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2007-12-20-tax-legislation-enacted&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Wed, 19 Dec 2007 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">526 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Parliament passes tax bills</title>
    <link>https://taxpolicy.ird.govt.nz/news/2007-12-12-parliament-passes-tax-bills</link>
    <description>&lt;p&gt;Parliament has passed tax legislation giving effect to the new R&amp;amp;D tax credit and KiwiSaver changes that include compulsory employer contributions and the employer tax credit. The changes were introduced in May in the Taxation (Annual Rates, Business Taxation, KiwiSaver, and Remedial Matters) Bill, which was divided into three bills at the Committee stage of proceedings. Other main reforms include changes to relax a whole range of tax penalties, increased tax incentives for making charitable donations, and provision for information matching between Inland Revenue and the New Zealand Customs Service for purposes of Child Support. The legislation now awaits Royal assent. For more information see the government&#039;s &lt;a href=&quot;/news/2007-12-12-parliament-passes-tax-bills#statement&quot;&gt;media statement&lt;/a&gt;.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2007-12-12-parliament-passes-tax-bills&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Tue, 11 Dec 2007 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">524 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  <item>
    <title>Select committee reports on tax bill</title>
    <link>https://taxpolicy.ird.govt.nz/news/2007-11-15-select-committee-reports-tax-bill</link>
    <description>&lt;p&gt;The Finance and Expenditure Committee has reported to Parliament on the Taxation (Annual Rates, Business Taxation, KiwiSaver, and Remedial Matters) Bill, which was introduced in May. Measures introduced in the omnibus bill include the 15% R&amp;amp;D tax credit, compulsory employer contributions to KiwiSaver and accompanying employer tax credit, increased tax incentives to promote charitable donations, and liberalisation of tax penalties to promote voluntary compliance.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;https://taxpolicy.ird.govt.nz/news/2007-11-15-select-committee-reports-tax-bill&quot; target=&quot;_blank&quot;&gt;read more&lt;/a&gt;&lt;/p&gt;</description>
     <pubDate>Wed, 14 Nov 2007 11:00:00 +0000</pubDate>
 <dc:creator>siteadministrator</dc:creator>
 <guid isPermaLink="false">509 at https://taxpolicy.ird.govt.nz</guid>
  </item>
  </channel>
</rss>