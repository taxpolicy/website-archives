<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://individuals.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of individuals’ income tax - Other proposals</title>
 <link>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals</link>
 <description>Encouraging use of special tax codes to get tax right during the year    
Inland Revenue wants to do more to encourage individuals to be on the right tax code during the year and would suggest a special code to an individual if it would benefit the individual. This will help ensure that people don’t end up with large bills or refunds at the end of the year.
The discussion document proposes that Inland Revenue would allow online applications for a special tax code, and would inform the employer when a special tax code is granted, so the individual no longer needs to do it.
This proposal would benefit people with multiple sources of income, such as someone with two jobs, or someone receiving income support and wages.
Claiming donations tax credits through the tax return process
People who use Payroll Giving for their charitable donations can obtain the tax credit immediately. This will not change.
People who make donations through other means must claim the tax credit at the end of the year by filling in an Inland Revenue form IR526.  About 80 percent of the people who fill in the form to seek a tax credit are also filling in other Inland Revenue tax return forms.
Claiming donations would be incorporated into the income tax process, so most people would no longer need to fill in two separate forms.  People who still wanted to fill in a separate form could continue to do so.
Direct crediting of income tax refunds
At present refunds are sent by cheque or directly deposited in the individual’s bank account.  Direct crediting is much faster, so the individual gets access to their money sooner.  It also costs less for the individual and for Inland Revenue.  If the current proposal is accepted it could apply to other groups of taxpayers, not just individuals.
The Government would like feedback on whether income tax refunds should be made only through direct crediting, with exemptions available in limited circumstances.  
</description>
 <language>en</language>
<item>
 <title>Do you support the proposal that income tax refunds should be made only by direct credit with limited exemptions being available?</title>
 <link>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-income-tax-refunds-should-be-made-only</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/other-proposals-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Other proposals&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 23 Mar 2017 19:31:31 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">205 at https://individuals.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-income-tax-refunds-should-be-made-only#comments</comments>
</item>
<item>
 <title>Do you support the proposal to allow people to claim donations credits without having to fill in a separate form? </title>
 <link>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-allow-people-claim-donations-credits</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/other-proposals-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Other proposals&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 23 Mar 2017 19:31:17 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">204 at https://individuals.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-allow-people-claim-donations-credits#comments</comments>
</item>
<item>
 <title>Do you agree that if an individual applies for a special tax code Inland Revenue should tell their employer what the code is?</title>
 <link>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-agree-if-individual-applies-special-tax-code-inland</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/other-proposals-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Other proposals&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 23 Mar 2017 19:30:43 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">203 at https://individuals.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-agree-if-individual-applies-special-tax-code-inland#comments</comments>
</item>
<item>
 <title>Do you support the proposal that Inland Revenue should suggest special tax codes, but not insist that an individual should use one? If not, why?</title>
 <link>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-inland-revenue-should-suggest-special-tax</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/other-proposals-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Other proposals&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 23 Mar 2017 19:30:27 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">202 at https://individuals.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://individuals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/other-proposals/do-you-support-proposal-inland-revenue-should-suggest-special-tax#comments</comments>
</item>
</channel>
</rss>
