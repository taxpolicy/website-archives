<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://payeandgst.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of PAYE and GST - Speed and accuracy of PAYE</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye</link>
 <description>What problems do current PAYE processes cause for employees?
Information employers provide to Inland Revenue is used to deliver various social policy functions, such as:
KiwiSaver
Working for Families entitlements
Income-tested benefit entitlements
Student loan repayments
Prompt and accurate PAYE information is important for these amounts to be calculated accurately. The current system has two limitations:
PAYE information is aggregated into monthly amounts, which mean it cannot be used to accurately calculate weekly or fortnightly income
PAYE information is sent to Inland Revenue in the month after payment was made to staff, therefore Inland Revenue is always working with out of date information.
Examples of some of the current problems arising are:
Employees whose secondary tax income takes them over an income rate threshold are at risk of having tax over-withheld.  If Inland Revenue had more timely PAYE information it could intervene more quickly, perhaps to suggest a special tax code.
More than 52,000 families were underpaid or overpaid Working for Families entitlements in the year to 30 June 2014, effective redevelopment of this system requires more timely accurate PAYE information.
18,700 student loan borrowers paid a “catch-up” repayment rate of 17%, rather than the standard rate of 12%, (year ending 30 June 2015) which could have been prevented if they been on the correct tax code. 
Problems which arise from Inland Revenue being unable to verify an individual’s identity. To reduce this problem there is a proposal that individuals supply date of birth information along with their tax code when they change employers.
Current State

Future State

 
</description>
 <language>en</language>
<item>
 <title>Should payroll software provide information about new employees to Inland Revenue when the information is entered into the payroll system?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/should-payroll-software-provide-information-about-new</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/72&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Speed and accuracy of PAYE&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;The Government proposes that where an employer uses a payroll system which connects to Inland Revenue&#039;s system, information about new employees could be provided when the relevant employee details were first entered into that payroll system.&lt;/p&gt;
&lt;p&gt;That would mean the employer could be provided with confirmation of the employee’s IRD number and tax code, and advised of the employee’s KiwiSaver status and deduction rate, and of any additional deductions (for example for child support) required, before the first time the employee was paid. This would eliminate at source errors which now take time to correct, and remove the need for a separate KiwiSaver form.&lt;/p&gt;
&lt;p&gt;The payroll system could also be used to advise Inland Revenue of updates to employees’ details, such as changes to their contact details or ceasing employment. This information would also be passed from Inland Revenue to the employee, so they could correct any errors.&lt;/p&gt;
&lt;p&gt;Questions&lt;/p&gt;
&lt;p&gt;1.  Do you agree with the proposal that employers should be able to use their payroll software to provide relevant employee details to Inland Revenue at the time the employee’s details are entered, changed in, or removed from the payroll system?&lt;/p&gt;
&lt;p&gt;2.  Would using payroll software to provide IR with details of new employees before they are first paid and being notified of deductions as set out above, reduce or increase compliance costs? If you can quantify the effect please do so.&lt;/p&gt;
&lt;p&gt;3.  Do you support the proposal that Inland Revenue should continue to communicate any change of employee obligations or details to the employee?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 09 Nov 2015 19:53:45 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">337 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/should-payroll-software-provide-information-about-new#comments</comments>
</item>
<item>
 <title>Should employees provide date of birth information to Inland Revenue, via their employer?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/should-employees-provide-date-birth-information-inland</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/72&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Speed and accuracy of PAYE&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Security and privacy concerns were raised in previous consultations.  Problems can arise in the tax system where people have the same name as others, use different spellings or versions of their own names, or make errors when they provide IRD numbers. These problems include deductions being wrongly credited and obligations wrongly assigned.&lt;/p&gt;
&lt;p&gt;Confusion over identity would be significantly reduced if the information new employees provided to their employer, to be forwarded to Inland Revenue, included their date of birth.    &lt;/p&gt;
&lt;blockquote&gt;&lt;h4&gt;&lt;strong&gt;Example: Michael new employee&lt;/strong&gt; &lt;/h4&gt;
&lt;h4&gt;Michael started his first job in April. His father’s name is also Michael and when the young Michael gives the payroll officer his IRD number he provided his father’s number by mistake – it was the first one he found in the desk drawer where the family papers were stored. &lt;/h4&gt;
&lt;h4&gt;At year end his father requested a PTS because he thought he was probably due a refund for the short period he had had between jobs. He was amazed to find that his income was reported as $31,000 higher than he thought and that he had significant tax to pay.   Although the problem got sorted out relatively quickly once he contacted Inland Revenue it caused considerable stress in the interim.  If Michael had had to provide his date of birth along with his IRD number the problem could have been avoided.&lt;/h4&gt;
&lt;/blockquote&gt;
&lt;h3&gt;&lt;strong&gt;Questions&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;1.  Do you agree with the proposal that employers should obtain date of birth information and provide this information about their new employees to Inland Revenue?&lt;/p&gt;
&lt;p&gt;2.  Should the requirement on the employee to provide date of birth information be included in legislation?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 02 Nov 2015 20:07:41 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">327 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/should-employees-provide-date-birth-information-inland#comments</comments>
</item>
<item>
 <title>Do current PAYE processes cause other problems for employees which should be addressed?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/do-current-paye-processes-cause-other-problems-employees</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/72&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Speed and accuracy of PAYE&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Information employers provide to Inland Revenue is used to deliver various social policy functions, such as:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;KiwiSaver&lt;/li&gt;
&lt;li&gt;Working for Families entitlements&lt;/li&gt;
&lt;li&gt;Income-tested benefit entitlements&lt;/li&gt;
&lt;li&gt;Student loan repayments&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Prompt and accurate PAYE information is important for these amounts to be calculated accurately. The current system has two limitations:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;PAYE information is aggregated into monthly amounts, which mean it cannot be used to accurately calculate weekly or fortnightly income&lt;/li&gt;
&lt;li&gt;PAYE information is sent to Inland Revenue in the month after payment was made to staff, therefore Inland Revenue is always working with out of date information.&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Examples of some of the current problems arising are:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;More than 52,000 families were underpaid or overpaid Working for Families entitlements in the year to 30 June 2014, effective redevelopment of this system requires timely, accurate PAYE information.&lt;/li&gt;
&lt;li&gt;18,700 student loan borrowers paid a “catch-up” repayment rate of 17%, rather than the standard rate of 12%, because of under deductions (year ending 30 June 2015) which could have been prevented if they had been on the correct tax code.&lt;/li&gt;
&lt;li&gt;This is an example of a problem which can arise because employers have incomplete or incorrect information when they first start to pay staff.  There is a proposal that employers should send key employee information to Inland Revenue before they start to pay staff, this would help to get deductions set up correctly from the start.&lt;/li&gt;
&lt;li&gt;Problems which arise from Inland Revenue being unable to verify an individual’s identity. To reduce this problem there is a proposal that individuals supply &lt;a href=&quot;/taxonomy/term/59#date of birth&quot;&gt;date of birth&lt;/a&gt;  information along with their tax code when they change employers.&lt;/li&gt;
&lt;/ul&gt;&lt;h3&gt;&lt;em&gt;Current State&lt;/em&gt;&lt;/h3&gt;
&lt;p&gt;&lt;img alt=&quot;&quot; height=&quot;700&quot; src=&quot;/sites/payeandgst/files/u71/paye_gst_images_studentloan_current.png&quot; width=&quot;700&quot; /&gt;&lt;/p&gt;
&lt;h3&gt;&lt;em&gt;Future State&lt;/em&gt;&lt;/h3&gt;
&lt;p&gt;&lt;img alt=&quot;&quot; height=&quot;700&quot; src=&quot;/sites/payeandgst/files/u71/paye_gst_images_studentloan_future.png&quot; width=&quot;448&quot; /&gt;&lt;/p&gt;
&lt;p&gt; &lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Question&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;Do current PAYE processes cause other problems for employees which should be addressed?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 05 Oct 2015 02:47:42 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">264 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/speed-and-accuracy-paye/do-current-paye-processes-cause-other-problems-employees#comments</comments>
</item>
</channel>
</rss>
