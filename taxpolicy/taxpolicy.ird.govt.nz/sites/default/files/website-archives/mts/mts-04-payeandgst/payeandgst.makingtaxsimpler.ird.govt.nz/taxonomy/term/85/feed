<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://payeandgst.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of PAYE and GST - Legislated rate changes</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/legislated-rate-changes</link>
 <description>Occasionally the Government changes tax rates or rates of other deductions or payments made by employers (such as KiwiSaver and student loan). These rate changes are made in legislation and take effect in a variety of different ways:
Legislated rate change
How the change takes effect

Tax on salary and wages


When a rate changes during a pay period and the pay period is one month or less, the new rate applies to the entire pay period.
When the pay period is more than one month, the payment is apportioned.
When the payment is after a rate change but relates to a period before it, the old rates apply.

Tax on extra pays

The tax rate applied to an extra pay is the relevant rate in force at the time the payment is made.

Student loan deductions

The rules are the same as the rules for salary and wages.

ACC earners&#039; levy

The rate is set in regulations, and applies to liable earnings for pay periods ending in the applicable tax year.

KiwiSaver contributions (employer and employee)
New rates apply from the first pay period starting after 1 April.
ESCT

The tax rate applied to the employer&#039;s superannuation contribution tax  is the relevant rate in force at the time the payment is made.

 
The Government thinks that employers’ processes would be simplified if the rules were aligned.
Options are:
the pay date
the pay period end-date
the pay period start-date
apportionment
Not all payments relate to a specific pay period (for example, extra pays). This points towards a pay date based approach, if alignment is seen as a preferred option.
Pay date is also the simplest option. However it might sometimes be inaccurate. For example, if the income tax rate changed from 30% to 20% in the middle of the year, a composite rate of 25% would apply for the entire year. An employee who was paid in arrears could have a little too much tax withheld at 20%, and if filing a return, would be likely to have a tax liability. Apportionment would deliver a more accurate result here. However, a pay date basis is more accurate where the rate change occurs at the beginning of a tax year.
</description>
 <language>en</language>
<item>
 <title>How do you think legislated rate changes should be dealt with?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/legislated-rate-changes/how-do-you-think-legislated-rate-changes-should-be-dealt</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/85&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Legislated rate changes&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Occasionally the Government changes tax rates or rates of other deductions or payments made by employers (such as KiwiSaver and student loan). These rate changes are made in legislation and take effect in a variety of different ways:&lt;/p&gt;
&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; height=&quot;448&quot; width=&quot;704&quot;&gt;&lt;thead&gt;&lt;tr&gt;&lt;th scope=&quot;col&quot;&gt;Legislated rate change&lt;/th&gt;
&lt;th scope=&quot;col&quot;&gt;How rate change takes effect&lt;/th&gt;
&lt;/tr&gt;&lt;/thead&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td&gt;Tax on salary and wages&lt;/td&gt;
&lt;td&gt;
&lt;p&gt;When a rate changes during a pay period and the pay period is one month or less, the new rate applies to the entire pay period.&lt;/p&gt;
&lt;p&gt;When the pay period is more than one month, the payment is apportioned.&lt;/p&gt;
&lt;p&gt;When the payment is after a rate change but relates to a period before it, the old rates apply.&lt;/p&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;Tax on extra pays&lt;/td&gt;
&lt;td&gt;The tax rate applied to an extra pay is the relevant rate in force at the time the payment is made.&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;
&lt;p&gt;Student loan deductions&lt;/p&gt;
&lt;/td&gt;
&lt;td&gt;
&lt;p&gt;The rules are the same as the rules for regular salary and wages.&lt;/p&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ACC earners&#039; levy&lt;/td&gt;
&lt;td&gt;
&lt;p&gt;The rate is set in regulations, and applies to liable earnings for pay periods ending in the applicable tax year.&lt;/p&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;KiwiSaver contributions (employer and employee)&lt;/td&gt;
&lt;td&gt;New rates apply from the first pay period starting after 1 April.&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ESCT&lt;/td&gt;
&lt;td&gt;
&lt;p&gt;The tax rate applied to employer&#039;s superannuation contribution tax is the relevant rate in force at the time the payment is made.&lt;/p&gt;
&lt;/td&gt;
&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;p&gt; &lt;/p&gt;
&lt;p&gt;The Government thinks that employers’ processes would be simplified if the rules were aligned.&lt;/p&gt;
&lt;p&gt;Options are:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;the pay date&lt;/li&gt;
&lt;li&gt;the pay period end-date&lt;/li&gt;
&lt;li&gt;the pay period start-date&lt;/li&gt;
&lt;li&gt;apportionment&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Pay date is the simplest option. However it might sometimes be inaccurate. For example, if the income tax rate changed from 30% to 20% in the middle of the year, a composite rate of 25% would apply for the entire year. An employee who was paid in arrears could have a little too much tax withheld at 20%, and if filing a return, would be likely to have a tax liability. Apportionment would deliver a more accurate result here. However, a pay date basis is more accurate where the rate change occurs at the beginning of a tax year.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Questions   &lt;/strong&gt;      &lt;/h3&gt;
&lt;p&gt;1.  Do you think that legislated rate changes could be applied in the same way across PAYE-related tax types/products?&lt;/p&gt;
&lt;p&gt;2.  Do you agree that a pay date approach is the best option for alignment?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 19 Oct 2015 21:19:57 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">286 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/legislated-rate-changes/how-do-you-think-legislated-rate-changes-should-be-dealt#comments</comments>
</item>
</channel>
</rss>
