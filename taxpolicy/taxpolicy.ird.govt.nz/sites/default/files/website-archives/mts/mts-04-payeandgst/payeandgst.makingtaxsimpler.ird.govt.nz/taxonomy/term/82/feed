<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://payeandgst.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of PAYE and GST - Moving to electronic transfer of information</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/moving-electronic-transfer-information</link>
 <description>Moving to accounting systems interacting with IR systems for GST
Transferring GST information through electronic channels is the best way to ensure information is timely and accurate, and to reduce compliance costs.
It is important that no-one is excluded by the move to accounting systems interacting with IR systems. The electronic channels which Inland Revenue provides now will continue to be available alongside the new accounting system-based digital channels. Paper filing will also remain available for the foreseeable future.
Inland Revenue has already started working with software developers and a pilot is expected to start in December 2015.
Adopting the use of accounting systems which interact with IR systems will be voluntary, for the foreseeable future at least.
Should moving to the new system be voluntary?
Electronic filing for larger businesses
The previous section suggests that it be voluntary to adopt the new approach of GST filing being carried out by accounting systems interacting directly with Inland Revenue.
However, Government is interested in views on whether some larger filers should be required to file using an electronic channel (this includes any available electronic channel, existing or new) instead of being able to file paper returns.
Filing electronically through channels other than the proposed new accounting system based channel does not provide the full benefit to customers and Inland Revenue. The transfer of information in electronic form nevertheless has the potential to remove some error sources, improve the integrity of the overall system, the speed with which information can be processed, and reduce both customers’ and Inland Revenue’s costs.
Should some businesses be required to file GST electronically?
</description>
 <language>en</language>
<item>
 <title>Should some businesses be required to file GST electronically?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/moving-electronic-transfer-information/should-some-businesses-be-required-file</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/82&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Moving to electronic transfer of information&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;The previous section suggests that it be voluntary to adopt the new approach of GST filing being carried out by accounting systems interacting directly with Inland Revenue. However, the transfer of information through any electronic channels, has the potential to remove some error sources, improve the integrity of the overall system and the speed with which Inland Revenue can process the information, and reduces both customers’ and Inland Revenue costs.&lt;/p&gt;
&lt;p&gt;The Government is therefore interested in views on whether some larger filers should be required to file using electronic channels – (this includes any available electronic channel, existing or new) instead of being able to file paper returns.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Questions&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;1.  Do you think GST registered persons over a certain threshold should be required to submit their GST information to Inland Revenue in an electronic format? &lt;/p&gt;
&lt;p&gt;2.  What do you think would be an appropriate threshold?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Sun, 18 Oct 2015 23:17:34 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">280 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/moving-electronic-transfer-information/should-some-businesses-be-required-file#comments</comments>
</item>
<item>
 <title>Should moving to the new system be voluntary?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/moving-electronic-transfer-information/should-moving-new-system-be-voluntary</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/82&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Moving to electronic transfer of information&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;It is important that no-one is excluded by the move to accounting systems interacting with IR systems. Paper filing will remain available for the foreseeable future. Simple electronic solutions will be available for those for whom the cost of software may be a barrier.&lt;/p&gt;
&lt;p&gt;Inland Revenue has already started working with software developers and a pilot is expected to start in December 2015.&lt;/p&gt;
&lt;p&gt;Adoption of the use of accounting systems which interact with IR systems will be voluntary, for the foreseeable future at least.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Question&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;Do you support the proposal that adopting the new services should be voluntary for GST information?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Sun, 18 Oct 2015 23:13:19 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">279 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/moving-electronic-transfer-information/should-moving-new-system-be-voluntary#comments</comments>
</item>
</channel>
</rss>
