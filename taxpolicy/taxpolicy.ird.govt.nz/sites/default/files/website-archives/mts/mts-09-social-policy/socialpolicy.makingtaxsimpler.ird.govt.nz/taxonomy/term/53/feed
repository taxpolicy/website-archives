<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://socialpolicy.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of social policy - Impacts on people with fluctuating income</title>
 <link>https://socialpolicy.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/impacts-people-fluctuating-income</link>
 <description>A main proposal in the discussion document is to base Working for Families payments on recent actual income.  Rather than being based on annual income and spread evenly across the year, Working for Families payments would adapt more quickly to changes in a family’s income and more closely match their need for support.
If a family’s income fluctuates a lot, the proposed changes mean that payments may be more or less than under the current system.  The difference in the payments would be larger if Inland Revenue looks back more often (a shorter period of assessment) or smaller if Inland Revenue looks back over a longer period.

A longer period of assessment means:


A shorter period of assessment means:

fluctuating income is less relevant, as highs and lows in weekly income are offset against each other, meaning payment amounts are more consistent

fluctuating income is more relevant, as high and low income periods will change  the amount of payment more frequently

This table shows the potential impact of looking back every month for a family earning $62,000 per year with one young child with income that varies month to month. 
 
Income
Working for Families now
Working for Families proposed
June
$3605
$216
$478
July
$8445
$216
$0
The payments would more closely match variations in income, with more paid after periods of low income and less paid after high periods of income.
The impact is mainly on those people who earn over the abatement threshold (currently $36,350 per year or $3030 per month) and who are still entitled to a payment.  Payments would more closely match their income in recent periods, but the person could receive more or less over the 12 month period, compared with the current annual system.
Whether the family receives more or less depends on the amount of income the family received in the period of assessment compared to the abatement threshold and the point at which payments are fully abated.
If a family has income that is below the abatement threshold for some periods and above the abatement threshold in other periods, they would receive less in 12 months compared with the current annual system.  This would most likely affect families whose income is near the abatement threshold.
The Government has said that nobody will be entitled to less Government support than they are now. These families would receive a catch-up payment to make sure they received their full annual entitlement based on their end-of-year income.
For a family with income in some periods above the point where the entitlement is fully abated, and some periods below this point, they would receive more than the current annual system.
</description>
 <language>en</language>
</channel>
</rss>
