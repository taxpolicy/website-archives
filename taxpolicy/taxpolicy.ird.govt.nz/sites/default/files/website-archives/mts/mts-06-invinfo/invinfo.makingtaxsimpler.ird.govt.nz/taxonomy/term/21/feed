<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://invinfo.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Investment Income Information - Date of birth information</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/date-of-birth-information</link>
 <description>The Government proposes that date of birth information should be provided to Inland Revenue if it is held by investment income payers. Inland Revenue currently receives IRD number, name and address information only as identifying information.
Some people use multiple spellings or versions of their name and it isn’t uncommon for two or more people to have the same name. People can also give incorrect IRD numbers by mistake.
Getting date of birth information as well will improve Inland Revenue’s ability to match income to a taxpayer, and make it possible to identify people even if they have had a change of name or address.

Example
Jenny and her mother have the same name and live at the same address. Jenny hasn’t given her interest payer her IRD number, but her mother has supplied hers. There is a risk that Jenny’s investment income could be matched with her mother’s Inland Revenue records. Inland Revenue could allocate their income to the right accounts if their dates of birth were provided by their investment providers.

People who responded to the Government’s Green Paper raised concerns about providing date of birth information, however these concerns largely related to employers receiving date of birth information for fears it could result in age discrimination.   These same concerns don’t hold for payers of investment income receiving date of birth information, and receiving this information would significantly increase Inland Revenue’s confidence in confirming the identity of an income earner. There was also a proposal to collect date of birth information for PAYE purposes in the Making Tax Simpler – Better Administration of PAYE and GST discussion document.
</description>
 <language>en</language>
<item>
 <title>Do you think investment income payers should provide Inland Revenue with date of birth information? </title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/date-of-birth-information/do-you-think-investment-income-payers-should-provide</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/date-of-birth-information-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Date of birth information&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Do you think investment income payers should provide Inland Revenue with date of birth information? If you don’t think so, please explain why.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Tue, 17 May 2016 01:50:25 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">164 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/date-of-birth-information/do-you-think-investment-income-payers-should-provide#comments</comments>
</item>
</channel>
</rss>
