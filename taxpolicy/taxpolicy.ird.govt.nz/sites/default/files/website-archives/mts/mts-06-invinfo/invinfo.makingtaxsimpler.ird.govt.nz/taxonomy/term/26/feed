<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://invinfo.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Investment Income Information - Information would be provided more often to Inland Revenue</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue</link>
 <description>It is proposed that Inland Revenue receives information about each recipient in the month following the month in which the income is paid, instead of after the end of the tax year. Due dates for filing returns and making payments will remain the same:
the 20th of the following month for resident withholding tax (RWT), non-resident withholding tax (NRWT) and the approved issuer levy (AIL) scheme, and
the end of the month for portfolio investment entities (PIEs).
The only difference is that information about individual taxpayers will also be required at this time, rather than just summary information.


PIE funds would provide information they already hold more often
Because a PIE needs to pay tax when an investor exits, the majority of PIEs attribute income to investors every day. So requiring PIEs to report investors’ income details and prescribed investor rates (PIRs) monthly would simply be asking for information they already hold. Filing dates would remain the same, i.e. the end of the month.
PIE income that is “locked in”, such as income from KiwiSaver funds and complying superannuation funds with withdrawal restrictions, isn’t included in income calculations for social policy purposes, so this income may not need to be reported as frequently.  For locked in funds it is proposed that PIE funds provide reporting on investors’ details perhaps quarterly or six-monthly, so Inland Revenue could determine whether the investors were on the correct PIRs.
</description>
 <language>en</language>
<item>
 <title>Would it make a significant difference to compliance costs to provide information about individual taxpayers monthly rather than quarterly, or would the difference be marginal as system changes would be needed either way?</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/would-it-make-a</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information would be provided more often to Inland Revenue&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Would it make a significant difference to compliance costs to provide information about individual taxpayers monthly rather than quarterly, or would the difference be marginal as system changes would be needed either way?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Sun, 26 Jun 2016 19:28:56 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">188 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/would-it-make-a#comments</comments>
</item>
<item>
 <title>What alternatives do you think should be considered for locked in PIEs for reporting investor details, PIRs and income?</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/what-alternatives-do</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information would be provided more often to Inland Revenue&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;What alternatives do you think should be considered for locked in PIEs for reporting investor details, PIRs and income?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Sun, 26 Jun 2016 19:28:01 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">187 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/what-alternatives-do#comments</comments>
</item>
<item>
 <title>If you are a manager of a PIE fund (or funds), would your systems be able to provide monthly income calculations and investor details?</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/if-you-are-a-manager</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information would be provided more often to Inland Revenue&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;If you are a manager of a PIE fund (or funds), would your systems be able to provide monthly income calculations and investor details?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Tue, 17 May 2016 02:08:34 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">173 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/if-you-are-a-manager#comments</comments>
</item>
<item>
 <title>What do you think is a realistic timeframe for making changes to systems?</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/what-do-you-think-is-a</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information would be provided more often to Inland Revenue&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;What do you think is a realistic timeframe for making changes to systems?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Tue, 17 May 2016 02:08:20 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">172 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-would-be-provided-more-often-to-inland-revenue/what-do-you-think-is-a#comments</comments>
</item>
</channel>
</rss>
