<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://invinfo.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Investment Income Information - Tax returns would be prepopulated</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/tax-returns-would-be-prepopulated</link>
 <description>Currently, information about individual taxpayers is only received after year end if at all
Currently, Inland Revenue doesn’t receive information about recipients’ interest or portfolio investment entity (PIE) income and the amount of tax that has been deducted from that income until after the end of the tax year. As a result:
Information is received too late to be pre-populated into tax returns.
Payers also provide the same information to their customers, who in turn provide it to Inland Revenue if they file a tax return. This is inefficient.
This means that anyone filing a return has to gather information about the interest, dividends and Māori authority distributions they have received from payers during the year and include it in their tax return.  Taxpayers with multiple investments can end up with many tax certificates that they need to keep track of to understand their tax position. This is time consuming and, if information is missed out, can result in incorrect returns being filed.
For dividends, Māori authority distributions and interest that is exempt from RWT or subject to AIL, Inland Revenue doesn’t receive information about recipients at all, unless it is asked for. 
In the future, information about individual taxpayers will be received more often
It is proposed that payers provide information about individual taxpayers to Inland Revenue in the month following the month in which the income is paid.  Inland Revenue will then be able to prepopulate tax returns with this information as it is received, providing taxpayers with:
a more up-to-date view of their tax affairs through logging onto the Inland Revenue website;
more time as they won’t have to find and then provide this information to Inland Revenue; and
less chance of filing an incorrect return.
</description>
 <language>en</language>
</channel>
</rss>
