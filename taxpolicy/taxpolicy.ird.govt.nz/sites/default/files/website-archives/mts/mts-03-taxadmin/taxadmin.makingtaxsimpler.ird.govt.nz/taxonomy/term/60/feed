<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Role of taxpayers and tax agents - Overview</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/role-taxpayers-and-tax-agents-overview</link>
 <description>To ease and simplify taxpayer self-assessment the Government intends to make better use of pre-populated tax returns  and automated business interactions between Inland Revenue systems and a business’s existing financial systems. 
These and other proposed changes that the Government intends to make as part of Making Tax Simpler will shape the role that taxpayers and tax agents fill in the modernised tax administration.

This consultation is focussed in particular on why the Government considers return pre-population and greater use of automated business interaction is a good idea; how these changes fit into the self-assessment model which underpins New Zealand’s tax administration and how a taxpayer’s obligations may change if they receive a pre-populated return or rely on automated business interaction.
In particular the discussion focusses on:
The role of taxpayers:
what responses would be required from a taxpayer or business who receives a pre-populated return or uses automated business interactions; 
what happens if the taxpayer fails to respond; and
how can information contained in the pre-populated return be amended?

The role of tax agents:
what the future role of tax agents looks like in the transformed administration;
what services Inland Revenue may want to offer to tax agents to better assist them in this role.

The Government is seeking feedback on:
the proposed process for outlining the responsibilities for individuals who receive a pre-populated return; 
the proposed process for confirming aggregate final figures for small businesses using the systems interaction to supply information automatically to Inland Revenue; and 
what services tax agents currently value, and what services they would like to see Inland Revenue offer. 
In moving to greater pre-population several other questions arise, for example who will receive the pre-populated returns and what information will they contain, which are outside of the scope of this consultation. This discussion does not directly address those questions but some initial responses are set out in other questions. These questions, and others, will be addressed and further specific details will be provided as part of a future Government discussion document on improving the tax system for individuals. 
 
 
</description>
 <language>en</language>
</channel>
</rss>
