<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Time bar</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/time-bar</link>
 <description>
When a taxpayer makes an assessment that is incorrect Inland Revenue can amend the assessment to increase the amount of tax, but only within the statutory “time bar”.  Outside of this period, currently four years from the end of the tax year in which the return was provided for income tax or GST assessments, the assessment is considered final.  The certainty the time bar provides is important for taxpayers given the complexity of tax law, the need to keep accurate records, and the consequences for a taxpayer if they fail to discharge their obligations (for example, penalties).  Equally, for the Government, a finite period has the advantage of providing a point in time against which to balance resources between current enforcement initiatives and encouraging future compliance. 
The period chosen for the time bar attempts to balance the obligation of taxpayers to assess the correct amount of tax with the desire for certainty of both taxpayers and the Government.  This four-year fixed-period approach (with some exceptions) is relatively simple as all parties generally know when the assessment will be considered final. 
The proposed features of the modernised tax administration, including increased use of technology in pre-populating tax returns for individuals, information matching and better compliance profiling may, however, give Inland Revenue a higher degree of comfort that a return is correct.
Once these features were implemented, the Government could consider a reduced time bar for Inland Revenue to make enquiries in situations when Inland Revenue was comfortable that the return was likely to be materially correct. A reduced time bar in these circumstances would achieve the desired certainty for taxpayers sooner and would allow Inland Revenue to focus available resources on collecting the highest net revenue over time in more effective ways.
What suggestions do you have for how the time bar could be developed in the modernised tax administration?



</description>
 <language>en</language>
<item>
 <title>What suggestions do you have for how the time bar could be developed in the modernised tax administration?</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/time-bar/what-suggestions-do-you-have-how-time-bar-could-be-developed-modernised</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/53&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Time bar&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;The proposed features of the modernised tax administration, including increased use of technology in pre-populating tax returns for individuals, information matching and better compliance profiling may, however, give Inland Revenue a higher degree of comfort that a return is correct.&lt;/p&gt;
&lt;p&gt;Once these features were implemented, the Government could consider a reduced time bar for Inland Revenue to make enquiries in situations when Inland Revenue was comfortable that the return was likely to be materially correct. A reduced time bar in these circumstances would achieve the desired certainty for taxpayers sooner and would allow Inland Revenue to focus available resources on collecting the highest net revenue over time in more effective ways.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Fri, 06 Nov 2015 01:01:46 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">213 at https://taxadmin.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/time-bar/what-suggestions-do-you-have-how-time-bar-could-be-developed-modernised#comments</comments>
</item>
</channel>
</rss>
