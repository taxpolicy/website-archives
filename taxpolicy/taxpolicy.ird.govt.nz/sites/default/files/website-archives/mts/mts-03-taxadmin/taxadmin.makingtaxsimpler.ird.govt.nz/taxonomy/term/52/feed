<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Advice and disputes procedures</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/advice-and-disputes-procedures</link>
 <description>In a self-assessment system taxpayers need to determine their tax obligations with reasonable certainty. To assist taxpayers to achieve this certainty, Inland Revenue provides various tools, such as information, reminders, self-assessment tools, online services and advice. Inland Revenue provides advice to the wider public through its call centres and community compliance. Inland Revenue also publishes interpretation statements, interpretation guidelines, public rulings, Questions We’ve Been Asked, Agents Answers, Business Tax Updates and other guides to assist taxpayers with their obligations.
As a result of some of the proposals in this and other Making Tax Simpler consultations, many taxpayer processes will be simplified, but the nature of tax law means that there will always be room for providing advice and support to assist compliance using any one of the methods just described.
The shift to a more modern tax administration may necessitate some change to the advice and disputes procedures, but the degree of change is still to be determined.  The Government wants to ensure that any changes to the advice and disputes regimes will maintain the focus on the key objectives of speed, accuracy and predictability in business taxation matters. 
The avenues for taxpayers to seek advice from Inland Revenue, and procedures for resolving disputes, are essential mechanisms that impact on these objectives and include:
seeking informal advice from Inland Revenue staff (for example contacting a call centre);
requesting non-binding informal advice by asking for Inland Revenue’s indicative view on how the law applies to a set of circumstances; or
seeking more formal advice by applying for a binding ruling which gives Inland Revenue’s binding view on how the law applies to a particular set of circumstances.
Sometimes a taxpayer may also seek to clarify how the law applies to them by initiating a dispute through the use of a taxpayer initiated notice of proposed adjustment.
 



 
</description>
 <language>en</language>
<item>
 <title>Are the current advice options working effectively? </title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/advice-and-disputes-procedures/are-current-advice-options-working-effectively</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/52&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Advice and disputes procedures&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;To guide future considerations, the Government seeks feedback on whether the current options for taxpayers to seek Inland Revenue’s view on specific issues are working effectively.&lt;/p&gt;
&lt;p&gt;If not, the Government welcomes readers’ views on how these products may be improved to better meet future needs taking account of the wider business transformation objectives, including proposals around the time bar and penalties.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Fri, 06 Nov 2015 00:58:30 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">211 at https://taxadmin.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/advice-and-disputes-procedures/are-current-advice-options-working-effectively#comments</comments>
</item>
</channel>
</rss>
