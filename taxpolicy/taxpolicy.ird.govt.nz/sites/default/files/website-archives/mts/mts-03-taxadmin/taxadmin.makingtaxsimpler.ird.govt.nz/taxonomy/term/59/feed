<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Tax secrecy - Overview</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/tax-secrecy-overview</link>
 <description>Tax secrecy or taxpayer confidentiality laws exist in most countries.  The tax secrecy rule in New Zealand is considerably broader in application than that of many other countries.  The Government considers some degree of change is needed to allow the modernised tax administration to be more efficient and to ensure Inland Revenue can have, where appropriate, a more active role in working with other government agencies.

Provisions protecting taxpayer confidentiality have been in place for over 130 years.  The secrecy rule requires that Inland Revenue officers must maintain secrecy on all matters relating to the Inland Revenue Acts, and must not communicate any matter other than for the purpose of carrying into effect that legislation.  The tax secrecy rule is supported by taxpayer confidentiality and forms a key role in maintaining the integrity of the tax system.
The coverage of the tax secrecy rule is not limited to taxpayer-specific information, rather it covers all information relating to the legislation administered by Inland Revenue.  This means that even statistical information or information that does not identify particular taxpayers is covered by tax secrecy and cannot be disclosed unless it is for tax purposes, or some specific exception is contained in the legislation.
While the tax secrecy rule has never been absolute, over time further exceptions have been added and there are now a number of exceptions to the general rule.  Many of these exceptions relate to disclosure of information to other government agencies. 
Rationale for tax secrecy
Traditionally tax secrecy was viewed as a means of improving compliance by reassuring taxpayers that the information provided to Inland Revenue would not be used for anything other than administering the tax system.
Another important rationale is that tax secrecy acts as a balance for the broad information gathering powers granted to Inland Revenue.  Revenue agencies are generally granted wide information collection powers so they can properly ensure that taxpayers are meeting their obligations.  It is sometimes said that the quid pro quo for these powers is that the information is subject to a strict rule of secrecy.
It has also been observed that compliance would be at risk unless all taxpayers know that Inland Revenue will act firmly with those who do not meet their obligations.  Otherwise compliant taxpayers will perceive a lack of integrity in the system and feel they are carrying an unfair burden.  This supports the use of information to call taxpayers to account as part of the general exception to tax secrecy – the “carrying into effect” of the Inland Revenue Acts.
Recently, the protection of taxpayer’s privacy has also featured as a justification for tax secrecy.  As with the other reasons however, this is balanced against the use of information to carry into effect the Inland Revenue Acts.
Tax secrecy in other countries
Most other countries require that tax agencies keep information confidential.  There is, however, significant variation across countries regarding the transparency of tax information, and the number of exceptions to the principle of secrecy or confidentiality. 
In Australia, Canada and the United States the tax secrecy rule is limited to information that identifies or could identify a taxpayer.  In the United Kingdom the secrecy rule is cast more broadly, however, information subject to tax secrecy is not exempt from the provisions of the Freedom of Information Act, other than where it identifies or could identify a taxpayer.
Other New Zealand legislation
While the Tax Administration Act plays the primary role in regulating Inland Revenue’s use and disclosure of information, other legislation including the Privacy Act, the Search and Surveillance Act and the Official information Act also apply. 
The purpose of the Official Information Act is to facilitate open government and the presumption is that government agencies will be as open as possible.  Similarly, the Privacy Act gives individuals the right to access personal information held by an agency about them.   The Privacy Act also sets out Information Privacy Principles, relating to the collection, use and disclosure of personal information, and contains specific frameworks for the sharing of information by government agencies.
While the Privacy Act and Official Information Act reflect broader government policy, it is clear that they do not override specific provisions in other legislation, including the Tax Administration Act.  Therefore, rights of access to information under both the Official Information Act and the Privacy Act are subject to the tax secrecy rule.
</description>
 <language>en</language>
</channel>
</rss>
