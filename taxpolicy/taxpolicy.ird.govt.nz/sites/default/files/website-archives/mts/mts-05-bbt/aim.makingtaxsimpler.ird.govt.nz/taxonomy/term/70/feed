<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://aim.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Provisional Tax Accounting Income Method - Large corporates using AIM</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim</link>
 <description>We are interested in whether larger corporate taxpayers would use AIM to pay provisional tax and, if so, how they would see it applying to them.
AIM has been designed primarily for small and medium sized businesses. The similarity of the software they use, combined with Inland Revenue’s ability to have an input into how the software is designed has created an opportunity to standardise a set of tax adjustments. 
By way of contrast, larger corporate taxpayers tend to have more bespoke systems designed to suit their businesses. It isn’t viable for Inland Revenue to audit each system to gain confidence in the accuracy of the provisional tax payment or have input into their creation. It is also thought that companies with a group structure would be excluded from AIM.
In our experience, most small businesses have minimal tax adjustments. This is in contrast to large corporate taxpayers who make more tax adjustments to their income to determine their end of year tax liability.
We are also aware that many larger corporates taxpayers have more complex tax adjustments that require a year end calculation and so can’t be applied against income during the year. Under AIM this would result in an increased proportion of adjustments in the final provisional tax payment, rather than these being spread throughout the year.
</description>
 <language>en</language>
<item>
 <title>Can AIM work with complex tax adjustments?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/can-aim-work-complex-tax-adjustments</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/large-corporates-using-aim-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Large corporates using AIM&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;We are also aware that many larger corporates have more complex tax adjustments that require a year end calculation and so can’t be applied against income during the year. Under AIM this would result in an increased proportion of adjustments in the final provisional tax payment, rather than these being spread throughout the year.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;How could the AIM model be extended to suit larger taxpayers and their increased complexity of tax adjustments?&lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 31 Mar 2016 21:29:09 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">259 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/can-aim-work-complex-tax-adjustments#comments</comments>
</item>
<item>
 <title>Does commonality of software matter?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/does-commonality-software-matter</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/large-corporates-using-aim-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Large corporates using AIM&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;AIM has been designed primarily for small and medium sized businesses. The similarity of the software they use combined with a proposed ability to have an input into how the software works has created an opportunity to standardise a set of tax adjustments. &lt;/p&gt;
&lt;p&gt;By way of contrast, larger taxpayers tend to have more bespoke systems designed to suit their businesses. It isn’t viable for Inland Revenue to audit each system to gain confidence in the accuracy of the provisional tax payment.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;How can Inland Revenue have confidence in the accuracy of payments calculated by larger taxpayers’ bespoke systems?&lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 31 Mar 2016 21:28:51 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">258 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/does-commonality-software-matter#comments</comments>
</item>
<item>
 <title>Would larger taxpayers want to use AIM?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/would-larger-taxpayers-want-use-aim</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/large-corporates-using-aim-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Large corporates using AIM&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;We are interested in whether large taxpayers would use AIM to pay provisional tax and, if so, how they would see it applying to them.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;If you are a larger taxpayer would you be interested in using AIM to pay your provisional tax payments?&lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 31 Mar 2016 21:28:24 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">257 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/large-corporates-using-aim/would-larger-taxpayers-want-use-aim#comments</comments>
</item>
</channel>
</rss>
