<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://aim.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Provisional Tax Accounting Income Method - Calculating tax adjustments</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/taxonomy/term/55</link>
 <description>Traditionally many tax adjustments are only identified and calculated at year end. However, most of the information required to calculate these adjustments is actually available when it is entered into software and doesn’t need to wait until year end. 
Software will be designed to help businesses and their advisors choose the correct tax treatment for a transaction. For example an alert could trigger saying “please check with your advisor” or “last year you coded this as…”. A tax advisor might be able set up an alert whenever something is coded to a particular account to review the treatment chosen by a business. For example if a business traditionally needs to review its legal fees in close detail, then as soon as an invoice is coded to the legal fees account, a notification could be sent to the advisor to review the treatment.
We are interested in your thoughts on the tax adjustments made throughout the year. In the detailed policy section we discuss the most common tax adjustments.  We welcome your input and views on how you currently treat them and how you think they could practically be treated throughout the year. We would also like your feedback in relation to your software, how can these adjustments be included into your software?
For more detailed policy analysis and further feedback opportunities in relation to the treatment of tax adjustments throughout the year click here
</description>
 <language>en</language>
<item>
 <title>What changes would you like to see to your software?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/what-changes-would-you-see-your-software</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/55&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Calculating tax adjustments&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Inland Revenue will be releasing software specifications that participating AIM software programs must meet. This will ensure that there is commonality in the treatment of the adjustments. We will be working with the software providers with regard to the enhancements made and would like to take your thoughts and view into account as we do this.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;/taxonomy/term/77&quot;&gt;For more detailed policy analysis and feedback opportunities in relation to making tax adjustments click here&lt;/a&gt;&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;How can your accounting software program best help you make your tax adjustments?&lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Tue, 13 Oct 2015 21:18:55 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">255 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/what-changes-would-you-see-your-software#comments</comments>
</item>
<item>
 <title>Will having a tax calculation in your accounting software program create problems?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/will-having-tax-calculation-your-accounting-software</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/55&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Calculating tax adjustments&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;AIM will give software providers the opportunity to include a tax calculation capability into their software. It will be designed to help businesses and their advisors choose the correct treatment. For example an alert could trigger saying “please check with your advisor” or “last year you coded this as…” A tax advisor might be able set up an alert so that whenever something is coded to a particular account, they get a notification to review the treatment chosen by a business.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;What major problems do you foresee with inclusion of the consideration of tax issues into your software package?&lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 12 Oct 2015 20:29:38 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">249 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/will-having-tax-calculation-your-accounting-software#comments</comments>
</item>
<item>
 <title>Can you do tax adjustments throughout the year?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/can-you-do-tax-adjustments-throughout-year</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/55&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Calculating tax adjustments&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Traditionally many tax adjustments are only identified and calculated at year end. However, most of the information required is available when it is entered into software and doesn’t need to wait until year end.  Therefore many tax adjustments could be entered during the year rather at year end.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;How will the expectation of considering your tax adjustments during the year work for you?   &lt;/strong&gt;&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 12 Oct 2015 20:28:19 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">248 at https://aim.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/calculating-tax-adjustments/can-you-do-tax-adjustments-throughout-year#comments</comments>
</item>
</channel>
</rss>
