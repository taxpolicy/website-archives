/**
 * Check local storage to see if the user has voted, and if they have not, to
 * hide the results, and use AJAX to retrieve the poll voting form.
 */

(function ($) {

Drupal.behaviors.elmo_poll = {
  attach: function (context, settings) {

    var _localStorageAvailable = isLocalStorageAvailable(),
        _keyPrefix = 'elmo_poll_';

    // Loop through each poll and see if a poll voting form needs to be
    // rendered.
    $('.node-poll').each(function(index) {
      var nid = $(this).data('nid');
      if (_localStorageAvailable) {

        // Define some variables.
        var $pollForm = $('.node-' + nid + ' .poll-voting-form');
        var $pollFormTab = $('.node-' + nid + ' .tab-vote');
        var $pollResults = $('.node-' + nid + ' .poll-results');
        var $pollResultsTab = $('.node-' + nid + ' .tab-results');
        var $pollMessages = $('.node-' + nid + ' .poll-messages');
        var $form = $('.node-' + nid + ' .poll-voting-form form');
        var enabledText = $form.find('input.form-submit').val();

        // Already voted, refresh the results table from the website.
        if (localStorage.getItem(_keyPrefix + nid)) {
          $pollForm.hide();
          $pollResultsTab.click();
          $pollFormTab.addClass('disabled').attr('data-toggle', '');
          updatePollResults(nid);
          $pollMessages.html(Drupal.theme('ajaxPollSuccess', Drupal.t('Your choice is already recorded')));
        }

        // Not yet voted, show the poll voting form, hide the results.
        else {
          $pollForm.show();
          $pollFormTab.click();

          // Set up the options for the AJAX voting mechanism.
          var options = {
            url: settings.elmo_poll.ajax_voting_path + '/' + nid,
            beforeSubmit: function(values, form, options) {
              $form.find('input.form-submit').attr('disabled', true).val(settings.elmo_poll.voting_message);
            },
            success: function(response, status) {
              // Remove previous messages and re-enable the buttons in case anything
              // goes wrong after this.
              $form.find('input.form-submit').attr('disabled', '').val(enabledText);
              $form.find('.messages').remove();

              // On success, replace the poll content with the new content.
              if (response.status) {
                $pollForm.hide();
                $pollResultsTab.click();
                $pollFormTab.addClass('disabled').attr('data-toggle', '');
                updatePollResults(nid);

                // Stop further votes.
                localStorage.setItem(_keyPrefix + nid, '1');

                // Display any new messages.
                if (response.messages) {
                  $pollMessages.html(Drupal.theme('ajaxPollSuccess', response.messages));

                  // Check for GA.
                  if (typeof ga == 'function') {
                    ga('send', 'event', 'poll', 'vote-success', 'Poll - ' + nid);
                  }
                }
              }

              // Poll vote was not recorded for some reason.
              else {
                if (response.messages) {
                  $pollMessages.html(Drupal.theme('ajaxPollError', response.messages));

                  // Check for GA.
                  if (typeof ga == 'function') {
                    ga('send', 'event', 'poll', 'vote-error', 'Poll - ' + nid);
                  }
                }
              }
            },
            complete: function(response, status) {
              $form.find('input.form-submit').attr('disabled', '').val(enabledText);

              if (status == 'error' || status == 'parsererror') {
                $pollMessages.html(Drupal.theme('ajaxPollError'));
              }
            },
            dataType: 'json',
            type: 'POST'
          };

          // Add the handlers to the Poll form through the jquery.form.js library.
          $form.ajaxForm(options);
        }
      }
    });
  }
};

/**
 * Simple check for local storage.
 */
function isLocalStorageAvailable() {
  if (!(JSON.stringify && JSON.parse)) {
    return false;
  }

  try {
    return 'localStorage' in window && window.localStorage !== null;
  }
  catch (pError) {
    return false;
  }
}

/**
 * Quickly update the poll results with AJAX.
 */
function updatePollResults(nid) {
  $.get(Drupal.settings.elmo_poll.ajax_results_path + '/' + nid, function(data) {
    $('.node-' + nid + ' .poll-results').html(data);
  });
}

/**
 * A fallback error that is shown upon a complete failure.
 */
Drupal.theme.prototype.ajaxPollError = function(msg) {
  return '<div class="alert alert-danger">' + msg || Drupal.settings.elmo_poll.error_message + '</div>';
};
Drupal.theme.prototype.ajaxPollSuccess = function(msg) {
  return '<div class="alert alert-success">' + msg + '</div>';
};

})(jQuery);
;
(function ($) {

Drupal.googleanalytics = {};

$(document).ready(function() {

  // Attach mousedown, keyup, touchstart events to document only and catch
  // clicks on all elements.
  $(document.body).bind("mousedown keyup touchstart", function(event) {

    // Catch the closest surrounding link of a clicked element.
    $(event.target).closest("a,area").each(function() {

      // Is the clicked URL internal?
      if (Drupal.googleanalytics.isInternal(this.href)) {
        // Skip 'click' tracking, if custom tracking events are bound.
        if ($(this).is('.colorbox')) {
          // Do nothing here. The custom event will handle all tracking.
          //console.info("Click on .colorbox item has been detected.");
        }
        // Is download tracking activated and the file extension configured for download tracking?
        else if (Drupal.settings.googleanalytics.trackDownload && Drupal.googleanalytics.isDownload(this.href)) {
          // Download link clicked.
          ga("send", "event", "Downloads", Drupal.googleanalytics.getDownloadExtension(this.href).toUpperCase(), Drupal.googleanalytics.getPageUrl(this.href));
        }
        else if (Drupal.googleanalytics.isInternalSpecial(this.href)) {
          // Keep the internal URL for Google Analytics website overlay intact.
          ga("send", "pageview", { "page": Drupal.googleanalytics.getPageUrl(this.href) });
        }
      }
      else {
        if (Drupal.settings.googleanalytics.trackMailto && $(this).is("a[href^='mailto:'],area[href^='mailto:']")) {
          // Mailto link clicked.
          ga("send", "event", "Mails", "Click", this.href.substring(7));
        }
        else if (Drupal.settings.googleanalytics.trackOutbound && this.href.match(/^\w+:\/\//i)) {
          if (Drupal.settings.googleanalytics.trackDomainMode != 2 || (Drupal.settings.googleanalytics.trackDomainMode == 2 && !Drupal.googleanalytics.isCrossDomain(this.hostname, Drupal.settings.googleanalytics.trackCrossDomains))) {
            // External link clicked / No top-level cross domain clicked.
            ga("send", "event", "Outbound links", "Click", this.href);
          }
        }
      }
    });
  });

  // Track hash changes as unique pageviews, if this option has been enabled.
  if (Drupal.settings.googleanalytics.trackUrlFragments) {
    window.onhashchange = function() {
      ga('send', 'pageview', location.pathname + location.search + location.hash);
    }
  }

  // Colorbox: This event triggers when the transition has completed and the
  // newly loaded content has been revealed.
  $(document).bind("cbox_complete", function () {
    var href = $.colorbox.element().attr("href");
    if (href) {
      ga("send", "pageview", { "page": Drupal.googleanalytics.getPageUrl(href) });
    }
  });

});

/**
 * Check whether the hostname is part of the cross domains or not.
 *
 * @param string hostname
 *   The hostname of the clicked URL.
 * @param array crossDomains
 *   All cross domain hostnames as JS array.
 *
 * @return boolean
 */
Drupal.googleanalytics.isCrossDomain = function (hostname, crossDomains) {
  /**
   * jQuery < 1.6.3 bug: $.inArray crushes IE6 and Chrome if second argument is
   * `null` or `undefined`, http://bugs.jquery.com/ticket/10076,
   * https://github.com/jquery/jquery/commit/a839af034db2bd934e4d4fa6758a3fed8de74174
   *
   * @todo: Remove/Refactor in D8
   */
  if (!crossDomains) {
    return false;
  }
  else {
    return $.inArray(hostname, crossDomains) > -1 ? true : false;
  }
};

/**
 * Check whether this is a download URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isDownload = function (url) {
  var isDownload = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  return isDownload.test(url);
};

/**
 * Check whether this is an absolute internal URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternal = function (url) {
  var isInternal = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return isInternal.test(url);
};

/**
 * Check whether this is a special URL or not.
 *
 * URL types:
 *  - gotwo.module /go/* links.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternalSpecial = function (url) {
  var isInternalSpecial = new RegExp("(\/go\/.*)$", "i");
  return isInternalSpecial.test(url);
};

/**
 * Extract the relative internal URL from an absolute internal URL.
 *
 * Examples:
 * - http://mydomain.com/node/1 -> /node/1
 * - http://example.com/foo/bar -> http://example.com/foo/bar
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   Internal website URL
 */
Drupal.googleanalytics.getPageUrl = function (url) {
  var extractInternalUrl = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return url.replace(extractInternalUrl, '');
};

/**
 * Extract the download file extension from the URL.
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   The file extension of the passed url. e.g. "zip", "txt"
 */
Drupal.googleanalytics.getDownloadExtension = function (url) {
  var extractDownloadextension = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  var extension = extractDownloadextension.exec(url);
  return (extension === null) ? '' : extension[1];
};

})(jQuery);
;
