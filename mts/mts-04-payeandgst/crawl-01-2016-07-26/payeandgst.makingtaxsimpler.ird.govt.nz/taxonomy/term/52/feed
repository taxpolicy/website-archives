<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://payeandgst.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of PAYE and GST - Employers</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/employers</link>
 <description></description>
 <language>en</language>
<item>
 <title>How do you think rate changes should be dealt with?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/employers/how-do-you-think-rate-changes-should-be-dealt</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/52&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Employers&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Occasionally the Government changes tax rates or rates of other deductions or payments made by employers (such as KiwiSaver and Child Support). These rate changes take effect on a variety of different dates:&lt;/p&gt;
&lt;table border=&quot;1&quot; cellpadding=&quot;1&quot; cellspacing=&quot;1&quot; style=&quot;width: 300px;&quot;&gt;&lt;tbody&gt;&lt;tr&gt;&lt;td&gt;Paye payments and Student loan deductions&lt;/td&gt;
&lt;td&gt;
&lt;p&gt;Where the rate changes during a pay period:&lt;/p&gt;
&lt;p&gt;and the pay period is one month or less, the new rate applies to the entire pay period&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;where the pay period is more than one month, the payment is apportioned&lt;/li&gt;
&lt;li&gt;Where the payment is after a date change but relates to a period before it, the old rates apply&lt;/li&gt;
&lt;/ul&gt;&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;KiwiSaver&lt;/td&gt;
&lt;td&gt;New rates apply from the first pay period starting after 1 April.&lt;/td&gt;
&lt;/tr&gt;&lt;/tbody&gt;&lt;/table&gt;&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt;PAYE payments&lt;/p&gt;
&lt;p&gt;and Student loan deductions&lt;/p&gt;
&lt;p&gt;Where the rate changes during a pay period:&lt;/p&gt;
&lt;p&gt;and the pay period is one month or less, the new rate applies to the entire pay period&lt;br /&gt;
where the pay period is more than one month, the payment is apportioned&lt;/p&gt;
&lt;p&gt;Where payment is after a date change but relates to a period before it, the old rates apply.&lt;/p&gt;
&lt;p&gt;KiwiSaver&lt;/p&gt;
&lt;p&gt;The Government thinks that employers’ processes would be simplified if the rules were aligned. Options are:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;the pay date&lt;/li&gt;
&lt;li&gt;the pay period end-date&lt;/li&gt;
&lt;li&gt;the pay period start-date&lt;/li&gt;
&lt;li&gt;apportionment&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;Pay date is the simplest option. However it might sometimes be inaccurate. For example, if the income tax rate changed from 30% to 20% in the middle of the year, a composite rate of 25% would apply for the entire year. An employee who was paid in arrears would have a little too much tax withheld at 20%, and if filing a return, would have a tax liability. Apportionment would deliver a more accurate result here. However, a pay date basis is more accurate where the rate change occurs at the beginning of a year.&lt;/p&gt;
&lt;p&gt;&lt;a href=&quot;/forum/62&quot;&gt;How do you think rate changes should be dealt with?&lt;/a&gt;&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Mon, 19 Oct 2015 03:11:13 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">283 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/employers/how-do-you-think-rate-changes-should-be-dealt#comments</comments>
</item>
</channel>
</rss>
