<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://payeandgst.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better administration of PAYE and GST - Extra pay day in a year</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/extra-pay-day-year</link>
 <description>Sometimes an extra pay day occurs in a tax year. For example, some tax years can have 53 weekly or 27 fortnightly pay days in them. When this happens, not enough tax is deducted because calculations are based on standard years of 52 weekly or 26 fortnightly pay days. People who file returns will typically end up with an unexpected tax liability. This could become a greater problem in the future if more people are required to file returns.
This issue could be addressed if tax tables published by Inland Revenue included an option for years with extra pay days, and software used to calculate PAYE determined whether an extra pay day would occur in the year and adjusted PAYE accordingly. The approach taken in Australia is to allow employees to ask their employers to make additional deductions in years with an extra pay day.
There are a range of different ways this could be implemented. One option would be to require employers to deduct additional amounts:
From all of their affected employees; or
Only from those employees who request additional withholding.
Alternatively, employers could have the option to choose whether to deduct additional amounts, and if they make this choice, would be required to deduct:
From all their affected employees; or
Only from those employees who request additional withholding.
</description>
 <language>en</language>
<item>
 <title>What do you think should be done in years with an extra pay day?</title>
 <link>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/extra-pay-day-year/what-do-you-think-should-be-done-years-extra-pay-day</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/70&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Extra pay day in a year&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Sometimes an extra pay day occurs in a tax year. For example, some tax years can have 53 weekly or 27 fortnightly pay days in them. When this happens, not enough tax is deducted because calculations are based on standard years of 52 weekly or 26 fortnightly pay days. People who file returns will typically end up with an unexpected tax liability. This could become a greater problem in the future if more people are required to file returns.&lt;/p&gt;
&lt;p&gt;This issue could be addressed if tax tables published by Inland Revenue included an option for years with extra pay days, and software used to calculate PAYE determined whether an extra pay day would occur in the year and adjusted PAYE accordingly. The approach taken in Australia is to allow employees to ask their employers to make additional deductions in years with an extra pay day.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Questions&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;Do you think a mechanism should be introduced for withholding extra amounts in years which have an extra pay day?&lt;/p&gt;
&lt;p&gt;If your answer is yes, which of the following approaches do you prefer?&lt;/p&gt;
&lt;p&gt;1.  Employers are required to deduct additional amounts from:&lt;/p&gt;
&lt;p&gt;               a)  All of their affected employees; or&lt;br /&gt;
               b)  Only those affected employees who request an additional withholding; or&lt;/p&gt;
&lt;p&gt;2.  Employers may choose whether to deduct additional amounts, and if they make this choice they will be required to deduct:&lt;/p&gt;
&lt;p&gt;               a)  From all of their affected employees; or&lt;br /&gt;
               b)  Only those affected employees who request an additional withholding.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Tue, 13 Oct 2015 22:48:32 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">277 at https://payeandgst.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://payeandgst.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/extra-pay-day-year/what-do-you-think-should-be-done-years-extra-pay-day#comments</comments>
</item>
</channel>
</rss>
