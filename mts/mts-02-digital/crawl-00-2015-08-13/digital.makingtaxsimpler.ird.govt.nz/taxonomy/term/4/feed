<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://digital.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better Digital Services - Using digital services</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services</link>
 <description>Would you move to digital services? 
Most customers are expected to move to digital services if a wide range of good quality services is developed, both by Inland Revenue and by the private sector in conjunction with Inland Revenue.  Reasons to support this are:

New Zealanders are quick to adopt new technology


There is already strong uptake of Inland Revenue&#039;s digital services.

Current state
Business customers need to interact with both their accounting software and Inland Revenue to meet their obligations

Future state
Business customers only need to interact once with their accounting software to meet their obligations
 

Customers are different 
Customers are different to each other: their needs, expectations of using digital services differ, as well as their reasons to interact with tax administration system.   This suggests that Inland Revenue will need to have a range of options to assist, support, encourage customers to move to better digital services.
Customer groups

Likely pattern of move to digital services
Most customers will voluntarily move to digital services, provided Inland Revenue builds, and works with the private sector to build a wide range of high quality digital services that meet customers’ needs.
Some customers will initially be unable to adopt digital services. Others will be able to adopt digital services if assistance is provided, such as providing offline support, kiosks or offering subsidies where appropriate. For the remainder, some non-digital services will need to be provided. These could either be provided directly by Inland Revenue or arrangements could be made for a third parties to supply them.
Some customers will be able to adopt digital services, but may choose not to. This may be an issue if the cost of providing non-digital services for them becomes prohibitive, or others (such as their customers or employees) are denied the benefits that a modernised tax administration could otherwise deliver. For both categories, the Commissioner of Inland Revenue could work alongside affected groups to encourage and support them to adopt digital services. The Commissioner has power under existing legislation to require the use of digital services if necessary. However, the Government thinks that where others are adversely affected by someone not using digital services, the Commissioner&#039;s requirements to use digital channels should have the full force of the law.
 
 
</description>
 <language>en</language>
<item>
 <title>Would you move to digital services if they met your needs?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/would-you-move-digital-services-if-they-met-your-needs</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;h3&gt;Do you think you would move to digital services for your tax interactions, if high-quality digital services which met your needs were offered?  &lt;/h3&gt;
&lt;p&gt;Digital services must be designed with the customer at the centre.  They will need to be easy to access and use, fast, secure, and reliable. A package of services which is designed for customers will need to be sufficiently flexible to keep pace with technology changes.&lt;/p&gt;
&lt;p&gt;Customer needs and their ability to use digital services differ, both by customer type and by the interaction which they are having with Inland Revenue. The requirements that a large business has for dealing with Inland Revenue about income tax are likely to be quite different from the requirements an individual will have. In these kinds of situations, it is likely that different types of digital services will need to be developed to provide a customer-focused experience and recognise that “no one size fits all”. In some instances new services may be better provided by third parties than directly by Inland Revenue.&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;
&lt;p&gt;Some customers’ needs will be best met through delivery by third parties:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;
&lt;p&gt;For businesses that already use computerised payroll systems, the best kind of digital service might be one delivered by the private sector. This could integrate PAYE filing into their payroll accounting software to remove the need for separate PAYE filing obligations.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;To integrate with existing processes, Inland Revenue would need to ensure it has close and collaborative working relationships with those who develop business accounting systems, and others who deal with individuals and businesses, such as financial institutions.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Interactions differ, as well as customers:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;
&lt;p&gt;Individuals and businesses will have different expectations of digital services. A person will have different expectations of customer-focused services, depending on the kind of tax interaction they require. Returning to the large business payroll example, direct integration with its payroll accounting system may be the best way to deal with the routine transfer of PAYE information.&lt;/p&gt;
&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;This may not be a business’s preferred mechanism for a non-routine interaction.  For example, if the Human Resources Manager and Chief Financial Officer become aware that the company’s previous PAYE treatment of redundancy payments has been incorrect they might want to discuss this concern directly with Inland Revenue, notwithstanding that their routine PAYE is dealt with automatically through their payroll software.&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;h3&gt;Can you foresee any interactions which you would not want to carry out using digital services? What would they be?&lt;/h3&gt;
&lt;p&gt;If individuals and businesses are offered good quality, secure, customer-focused digital services for interactions appropriately carried out across digital channels (including in partnership with third parties such as business accounting software providers), the majority can be expected to move to these services in the timeframe which best fits their own personal or business needs. They would likely do this because they identify the benefits that digital services would provide, such as greater convenience, certainty or reliability.&lt;/p&gt;
&lt;p&gt;Several facts support this view:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;The ability of New Zealand individuals and businesses to use digital solutions is relatively high. For example, New Zealand businesses already lead the world in the adoption of cloud-based services.&lt;/li&gt;
&lt;li&gt;Despite the constraints of aging technology, Inland Revenue’s ability to deliver quality digital services is recognised.&lt;/li&gt;
&lt;li&gt;Uptake of Inland Revenue’s digital services has, to date, been strong. For example, the online service for individuals, myIR, has 1.7 million active registered customers. In December 2014, 86% of payments to Inland Revenue were made through electronic channels.&lt;/li&gt;
&lt;/ul&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:38:33 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">64 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/would-you-move-digital-services-if-they-met-your-needs#comments</comments>
</item>
<item>
 <title>What could Inland Revenue do to support new customers to adopt digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-could-inland-revenue-do-support-new-customers-adopt</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;There are a number of strategies that Inland Revenue could develop to encourage new customers to adopt digital services. One could be to move to a “digital by default” strategy, in particular for new customers (or existing customers who are starting to use a new form of interaction – such as registering for GST).&lt;/p&gt;
&lt;p&gt; &lt;/p&gt;
&lt;p&gt; &lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:37:20 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">63 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-could-inland-revenue-do-support-new-customers-adopt#comments</comments>
</item>
<item>
 <title>What could Inland Revenue do to support existing customers to use digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-could-inland-revenue-do-support-existing-customers</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;h3&gt; &lt;/h3&gt;
&lt;p&gt;There are a number of strategies that Inland Revenue could develop to encourage customers to adopt digital services. One could be to move to a “digital by default” strategy. A good example of this would be to use email to communicate with customers who have consented to this form of communication.&lt;/p&gt;
&lt;p&gt;Where a high-quality digital service exists (whether delivered by the private sector or directly by Inland Revenue), customers should be directed to that option as a matter of course. Non-digital alternatives should be de-emphasised in favour of the digital offering. This is simpler for customers compared with the alternative of a customer adopting a non-digital option initially and subsequently facing the cost and disruption of moving to a digital option.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:35:59 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">62 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-could-inland-revenue-do-support-existing-customers#comments</comments>
</item>
<item>
 <title>Do you agree that specific assistance should be provided for some customers?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-specific-assistance-should-be-provided-some</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;h3&gt;Do you agree Inland Revenue should provide specific assistance to enable some customers to use digital services?&lt;/h3&gt;
&lt;h4&gt;&lt;em&gt;Some customers cannot move to digital services&lt;/em&gt;&lt;/h4&gt;
&lt;p&gt;Even if high-quality digital services targeted at all kinds of customers and appropriate tax interactions are built, there will inevitably be some customers who cannot adopt them.&lt;/p&gt;
&lt;p&gt;Reasons for this could include:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;they do not have the skills or knowledge to use digital technology;&lt;/li&gt;
&lt;li&gt;they do not have access to digital technology – they may be financially constrained or perhaps live in an area of New Zealand which does not have internet access.&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;For this group, the relevant principle is that tax compliance and access to entitlements are critical. Their ability to comply with their tax obligations and access their entitlements must remain even if the majority of other customers are carrying out those interactions through digital options.&lt;/p&gt;
&lt;p&gt;Two approaches could be offered to customers who cannot move to digital services. Both approaches would be operated simultaneously, with different customers falling into each approach. They are:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;Offer appropriate assistance (Principle: Tax compliance and access to entitlements are critical)&lt;/li&gt;
&lt;li&gt;
&lt;p&gt;Non-digital services should be available for some customers (Principle: Tax compliance and access to entitlements are critical)&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;&lt;h3&gt;What forms do you think that assistance might take?&lt;/h3&gt;
&lt;p&gt;The first approach is to provide some specific, targeted assistance to enable those customers to use digital services. Some examples of this kind of assistance could be:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;Recognition that online services require appropriate offline support – the New Zealand strategy for digital public services requires that it should be easy for people to get sufficient support when using digital options.&lt;/li&gt;
&lt;li&gt;Kiosks – Places where people who do not have internet access could go to use an internet-connected computer (whether provided by government or otherwise). Some specific assistance or even a digital service specifically designed for people who are not experienced internet users could even be provided.&lt;/li&gt;
&lt;li&gt;Assistance to develop digital capability – Inland Revenue’s officers already help customers, including small businesses, to understand their tax obligations and how to use existing digital services.&lt;/li&gt;
&lt;li&gt;Subsidies – For example, to support businesses adopting accounting software which integrates tax functions.&lt;/li&gt;
&lt;/ul&gt;&lt;p&gt;The second approach to this customer group is to acknowledge that, whatever support and assistance Inland Revenue or wider government might be able to offer, some customers will still not be able to directly use digital services. For example, a business in a remote location with no broadband internet access will not be able to use business accounting software which integrates tax obligations online even if the cost is subsidised. These customers will still need to manage their interactions with Inland Revenue through non-digital services.&lt;/p&gt;
&lt;p&gt;This could be done by ensuring existing non-digital services are provided. However, depending on the nature of some of Inland Revenue’s system changes, retaining existing services in their current form in the future may not be possible. In these cases, some process to convert non-digital to digital information might be required. Inland Revenue could carry out this process itself, or arrange for a third party to supply it.&lt;/p&gt;
&lt;h3&gt;How long do you think that assistance should be provided for?&lt;/h3&gt;
&lt;p&gt; &lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:34:27 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">61 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-specific-assistance-should-be-provided-some#comments</comments>
</item>
<item>
 <title>Who will not be able to move to digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/who-will-not-be-able-move-digital-services</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Who do you think will not be able to move to digital services, even with specific assistance?&lt;/p&gt;
&lt;p&gt;Whatever support and assistance Inland Revenue or wider government might be able to offer, some customers will still not be able to directly use digital services. For example, a business in a remote location with no broadband internet access will not be able to use business accounting software which integrates tax obligations online even if the cost is subsidised. These customers will still need to manage their interactions with Inland Revenue through non-digital services.&lt;/p&gt;
&lt;p&gt;This could be done by ensuring existing non-digital services remain available. However, depending on the nature of some of Inland Revenue’s system changes, retaining existing services in their current form in the future may not be possible.  In these cases, some process to convert non-digital to digital information might be required. Inland Revenue could carry out this process itself, or arrange for a third party to supply it.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:33:18 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">60 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/who-will-not-be-able-move-digital-services#comments</comments>
</item>
<item>
 <title>What should the approach be to customers who impose a cost on everyone by not using digital services? </title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-should-approach-be-customers-who-impose-cost-everyone</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;h3&gt;&lt;em&gt;Some customers could move to digital services, but will choose not to&lt;/em&gt;&lt;/h3&gt;
&lt;p&gt;These customers will not be subject to the constraints preventing adoption of digital services set out elsewhere but for whatever reason, have chosen not to move to digital services.&lt;/p&gt;
&lt;p&gt;Once Inland Revenue has developed a comprehensive portfolio of high-quality digital services, the majority of customers who can adopt those services are, over time expected to “vote with their feet” and move to digital services. The category of customers who can adopt those services but who choose not to is therefore expected to be small, and will diminish over time.&lt;/p&gt;
&lt;p&gt;It is likely that digital and non-digital services will sit alongside each other for some time, with the choice of channel being largely left to customers.&lt;/p&gt;
&lt;p&gt;The cost of delivering tax administration through digital services is typically lower than by non-digital equivalents. Because tax administration is funded out of government revenue, everyone bears the cost.  Everyone benefits by having the most efficient tax administration system.  Accordingly, allowing some customers to impose a cost on wider society because of a choice they made, rather than out of necessity would need careful consideration. &lt;/p&gt;
&lt;h3&gt;Do you agree that when some people have a digital service option available but by not using it are imposing a cost on everyone, they should be supported, encouraged and, if necessary, ultimately required to use digital services?&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:31:36 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">59 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/what-should-approach-be-customers-who-impose-cost-everyone#comments</comments>
</item>
<item>
 <title>Do you agree that the Commissioner should use her existing powers to facilitate the move to digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-commissioner-should-use-her-existing-powers</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Where customers could move to digital services but have chosen not to, the Commissioner of Inland Revenue would need to work alongside these customers to support and encourage them to adopt digital services. The Commissioner also has the ability to determine the range of services which are offered, under her powers to administer the tax system.&lt;/p&gt;
&lt;p&gt;Those powers are set out in section 6A of the Tax Administration Act 1994, and give the Commissioner the duty to collect the highest net revenue over time, having regard to:&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;the resources available to the Commissioner;&lt;/li&gt;
&lt;li&gt;the importance of promoting compliance; and&lt;/li&gt;
&lt;li&gt;the compliance costs incurred by taxpayers.&lt;/li&gt;
&lt;/ul&gt;&lt;h3&gt;Imposition of additional and avoidable costs on tax administration system&lt;/h3&gt;
&lt;p&gt;Section 6A requires the Commissioner to compare the additional costs these customers would impose on the tax system by not using digital services with the costs these customers would face in moving to digital services, and using those digital services on an ongoing basis. The Commissioner could make the decision under this provision to remove non-digital services for some customers for some transactions.&lt;/p&gt;
&lt;p&gt;As well as the additional and avoidable costs imposed on the tax system, the reluctance of these customers to use digital channels may also be imposing additional and avoidable costs on other agencies which rely on information provided by Inland Revenue. The Commissioner may also wish to take these costs into account in her approach to the use of digital channels by these customers.&lt;/p&gt;
&lt;h3&gt;Where others are denied the benefits of the new tax administration system&lt;/h3&gt;
&lt;p&gt;The Commissioner of Inland Revenue would need to work alongside those who provide information about the tax position of others, and who are being denied the benefit of the new tax administration system, to  support and encourage them to adopt digital services. Again, the Commissioner’s powers under the Tax Administration Act would allow her to restrict the availability of non-digital channels for some transactions carried out by those who provide information if this was appropriate, although this would need to be evaluated against compliance costs for those affected.&lt;/p&gt;
&lt;p&gt;The Government sees the satisfactory resolution of this issue as critical to ensuring that the New Zealand economy as a whole derives maximum benefit from the Government’s investment in the new tax administration system, and the delivery of this benefit to some sectors of the economy should not be prevented by the reluctance of others to adopt digital services.&lt;/p&gt;
&lt;p&gt;Accordingly, the Government proposes to make it clear that any decision by the Commissioner to require these customers to use digital channels has the full force of law.&lt;/p&gt;
&lt;h3&gt;Do you agree that the Commissioner should use her existing powers under the Tax Administration Act, which include the requirement to have regard to compliance costs, to faciliate the move to digital services?&lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:17:37 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">58 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-commissioner-should-use-her-existing-powers#comments</comments>
</item>
<item>
 <title>Do you agree some people should be required to use digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-some-people-should-be-required-use-digital</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/using-digital-services-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Using digital services&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;As Inland Revenue’s transformation programme progresses, the levels of service offered to everyone affected by the tax system in some way are expected to significantly improve. In some circumstances, those improvements will be contingent on Inland Revenue receiving information through digital channels – for example, employers, who provide PAYE information about their employees to Inland Revenue.&lt;/p&gt;
&lt;p&gt;PAYE information is critical to determining matters like social policy entitlements of employees. The tax system is likely to evolve so that Inland Revenue can deliver better services to these employees (such as correct calculations of Working for Families entitlements for each pay period, removing any need for annual square-ups), when their employers are working with Inland Revenue via digital services.&lt;/p&gt;
&lt;p&gt;Another example could be tax intermediaries, who provide information to Inland Revenue on the tax affairs of their clients.  Inland Revenue may be able to deliver better services to these clients – such as allowing them to check their tax position online in near real-time, when their tax agents are working with Inland Revenue via digital services.&lt;/p&gt;
&lt;p&gt;As noted above, as well as this information being critical to determine the tax position of others, it may also be used by other government agencies to determine other liabilities. For example, information provided by employers to Inland Revenue is used by ACC to determine liability for earner account levies for employees.&lt;/p&gt;
&lt;h3&gt;Do you agree that where some people (such as employers and tax agents) who choose not to use digital services, and by doing so are denying others (such as their employees or clients) the benefits of the new tax administration system, that first group should be required to use digital services?&lt;br /&gt;
 &lt;/h3&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Fri, 06 Mar 2015 04:14:46 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">57 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/using-digital-services/do-you-agree-some-people-should-be-required-use-digital#comments</comments>
</item>
</channel>
</rss>
