<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://digital.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Better Digital Services - Principles</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/principles</link>
 <description>Digital tax services must be designed and operated to be secure, reliable and based on best practice standards.  But developing and providing secure, reliable digital services does not mean that customers will use them.  It is important to have a number of principles to guide the development of digital services. 
 
Meeting customers&#039; needs

Principles
An overarching principle is that services must be designed for the customer. Each of the following principles addresses a different aspect of this overriding principle.
Principle 1: no one size fits all.
Customers, and types of interactions all vary. Technology will continue to change. To meet people&#039;s needs, it is important that these differences are understood and customers are offered the services that best meet their needs.
Principle 2: tax compliance and access to entitlements are critical.
The tax administration system is based on voluntary compliance, which will only work if customers can interact with the sytem. Similarly, those who access social policy entitlements through the tax administration system must have their ability to access to those entitlements preserved despite technology change.
Principle 3: change will not be imposed without careful consideration of the costs and benefits.
Interactions with the tax administration system create both compliance costs for customers and administration costs for the system itself. But customers do not directly bear administration costs and the tax administration system does not directly bear customers&#039; compliance costs. Both need to be carefully considered and balanced before changes are made.
</description>
 <language>en</language>
<item>
 <title>Do you agree with these principles to guide the development of digital services? </title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/principles/do-you-agree-these-principles-guide-development-digital-services</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/principles-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Principles&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Do you agree that the following principles are important when considering how greater use of digital technology might beneift all those who use the tax administration system?&lt;/p&gt;
&lt;p&gt;First, the overarching principle is that services must be designed for the customer. Each of the following principles addresses a different aspect of this overriding principle.&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;Principle 1: no one size fits all.&lt;/li&gt;
&lt;li&gt;Principle 2: tax compliance and access to entitlements are critical.&lt;/li&gt;
&lt;li&gt;Principle 3: change will not be imposed without careful consideration of the costs and benefits.&lt;/li&gt;
&lt;/ul&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Thu, 05 Mar 2015 22:15:13 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">51 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/principles/do-you-agree-these-principles-guide-development-digital-services#comments</comments>
</item>
<item>
 <title>Do you think there are any other principles which should guide the development of digital services?</title>
 <link>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/principles/do-you-think-there-are-any-other-principles-which-should-guide</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/principles-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Principles&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Apart from the principles stated below, do you think there are any other principles which ought to be taken into account?&lt;/p&gt;
&lt;p&gt;First, the overarching principle is that services must be designed for the customer. Each of the following principles addresses a different aspect of this overriding principle.&lt;/p&gt;
&lt;ul&gt;&lt;li&gt;Principle 1: no one size fits all.&lt;/li&gt;
&lt;li&gt;Principle 2: tax compliance and access to entitlements are critical.&lt;/li&gt;
&lt;li&gt;Principle 3: change will not be imposed without careful consideration of the costs and benefits.&lt;/li&gt;
&lt;/ul&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Wed, 11 Mar 2015 02:27:20 +0000</pubDate>
 <dc:creator>wei.deng</dc:creator>
 <guid isPermaLink="false">75 at https://digital.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://digital.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/principles/do-you-think-there-are-any-other-principles-which-should-guide#comments</comments>
</item>
</channel>
</rss>
