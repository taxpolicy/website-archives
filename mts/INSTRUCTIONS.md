Information for making a mirror of a site
=========================================

Revision history
-----------------------
1.0 - 2016-06-22 - Initial version with all notes in one place [David Nind]

Commands using wget
-------------------

Example using taxadmin.makingtaxsimpler.ird.govt.nz

Local mirror - command in full:

wget --mirror --convert-links --adjust-extension --page-requisites --no-parent --span-hosts --domains taxadmin.makingtaxsimpler.ird.govt.nz,fonts.googleapis.com,fonts.gstatic.com,html5shiv.googlecode.com --wait 1 --execute robots=off https://taxadmin.makingtaxsimpler.ird.govt.nz/

Local mirror - command in a shorter format:

wget -mkEpnp --span-hosts --domains taxadmin.makingtaxsimpler.ird.govt.nz,fonts.googleapis.com,fonts.gstatic.com,html5shiv.googlecode.com --wait 1 -e robots=off https://taxadmin.makingtaxsimpler.ird.govt.nz/

Create a warc file:

wget --warc-file IRD-taxadmin-CRAWL-01-201606220815 --warc-cdx --warc-header="operator: Policy and Strategy, Inland Revenue, New Zealand" -mkEpnp --span-hosts --domains taxadmin.makingtaxsimpler.ird.govt.nz,fonts.googleapis.com,fonts.gstatic.com,html5shiv.googlecode.com --wait 1 -e robots=off https://taxadmin.makingtaxsimpler.ird.govt.nz/

Usage:

--mirror (m): mirrors the whole site.

--convert-links (k): convert all the links (including JS and CSS stylesheets) to relative, so the site is available for offline viewing.

--adjust-extension (E): adds suitable extensions to filenames (html or css) depending on their content-type.

--page-requisites (p): download things like CSS style-sheets and images required to properly display the page offline.

--no-parent (np): download things like CSS style-sheets and images required to properly display the page offline.

--domains: includes external resources required to make the site work, for example fonts

--wait 1: delay each request by 1 second to be nice to the server

--user-agent="": some web servers disallow wget from crawling the site

--execute robots=off (-e robotes=off): ignore robots.txt files.

For warc file:

--warc-file=FILENAME: creates a warc file call FILENAME.
--warc-header=STRING; adds STRING as a custom header to the warcinfo record, e.g. "operator: Archive Team".
--warc-cdx=FILENAME: write a CDX index file to FILENAME.cdx. The CDX file contains a list of the records and their locations in the WARC files.

FILENAME for warc file and cdx (from WARC Guidelines v1):
IRD-taxadmin-CRAWL-01-yyyymmddhhmm.warc

STRING for warc header
"operator: Policy and Strategy, Inland Revenue, New Zealand"

Links and references - all accessed on 2016-06-21:
http://www.guyrutenberg.com/2014/05/02/make-offline-mirror-of-a-site-using-wget/
http://inkdroid.org/2016/04/14/warc-work/
http://www.netpreserve.org/sites/default/files/resources/WARC_Guidelines_v1.pdf [Downloaded]
http://archiveteam.org/index.php?title=Wget_with_WARC_output
http://archiveteam.org/index.php?title=Wget
https://www.drupal.org/node/27882
https://www.gnu.org/software/wget/
