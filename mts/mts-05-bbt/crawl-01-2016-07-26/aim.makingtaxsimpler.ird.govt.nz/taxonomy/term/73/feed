<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://aim.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Provisional Tax Accounting Income Method - How will AIM help me?</title>
 <link>https://aim.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/how-will-aim-help-me</link>
 <description>We have listened to your feedback with regard to provisional tax.  We understand the difficulties and problems that arise due to the lack of integration between the current provisional tax options and business rhythms and processes.  They don&#039;t easily reflect current income as it is earned due to fixed payment dates, and do not consider current year tax adjustments without exposure to use of money interest and penalties until the year-end tax return is being prepared - which can be a year after the activity occurred.  
AIM will change many of these.  Paying provisional tax through AIM means more regular payments based on current year tax adjusted income already earned, using software the business is already familiar with.  
Businesses using AIM will have more certainty they are paying the right amount of tax as it is paid in arrears. This will increase their confidence in their financial position at any particular time. More regular payments will remove stress and doubt about meeting a large tax liability at a later date.
Using  familiar software to calculate and pay tax will reduce time and effort for businesses. You won’t need to manually enter data into an online form, learn a new system or switch between two separate programs.
Currently in software, as expenses and income are coded, only their GST status is considered. Under AIM, the income tax treatment of income and expenses will also be a visible coding option.
Ensuring that paying income tax becomes part of running a business means that a business will always know what its true profit is, what its tax liability is, and what it can reinvest or return to its shareholders. 
As an aside, we know that the GST ratio method was intended to integrate GST and provisional tax, however it hasn’t been widely taken up due to restrictions placed on its use. The AIM method is intended to be kept simple and easy to use without being complicated by restrictions.  AIM does not replace the GST ratio method, which is still available.
</description>
 <language>en</language>
</channel>
</rss>
