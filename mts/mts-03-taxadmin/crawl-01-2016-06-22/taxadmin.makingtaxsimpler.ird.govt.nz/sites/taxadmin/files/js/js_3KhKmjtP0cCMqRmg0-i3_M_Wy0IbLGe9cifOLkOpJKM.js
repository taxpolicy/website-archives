(function ($) {

Drupal.behaviors.textarea = {
  attach: function (context, settings) {
    $('.form-textarea-wrapper.resizable', context).once('textarea', function () {
      var staticOffset = null;
      var textarea = $(this).addClass('resizable-textarea').find('textarea');
      var grippie = $('<div class="grippie"></div>').mousedown(startDrag);

      grippie.insertAfter(textarea);

      function startDrag(e) {
        staticOffset = textarea.height() - e.pageY;
        textarea.css('opacity', 0.25);
        $(document).mousemove(performDrag).mouseup(endDrag);
        return false;
      }

      function performDrag(e) {
        textarea.height(Math.max(32, staticOffset + e.pageY) + 'px');
        return false;
      }

      function endDrag(e) {
        $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);
        textarea.css('opacity', 1);
      }
    });
  }
};

})(jQuery);
;
/**
 * Check if the user has session Storage.
 * If so, allow the option to "Remember my name and email"
 * This prevents the user from having to enter their name and 
 * email in for every comment.
 *
 * Note, there are two comment forms on the page - 1 is a modal
*  This is why there is what appears to be repeats in the code below
 */

(function ($) {

  $( document ).ready(function() {

    // Define some variables.
    var cform = $('#comment-form');
    var cNameField = $('#edit-name');
    var cNameField2 = $('#edit-name--2'); // modal form version
    var cEmailField = $('#edit-mail');
    var submit = $('#edit-submit');
    var submit2 = $('#edit-submit--2');  // modal form version
    var rememberVal = '';
    var _sessionStorageAvailable = isSessionStorageAvailable();

    // Only proceed if sessionStorage is available and 
    // the name field exists (ie anonymous users)
    if (_sessionStorageAvailable && cNameField.length > 0) {

      // Populate form values
      populateFormValues()

      // Save/Delete sessionStorage vals when form is submitted
      submit.click(saveOrDeleteValues);
      submit2.click(saveOrDeleteValues);

    }

    /**
     * Populate form values based on previous saved details
     * Also add the "remember me" checkbox to the form
     */
    function populateFormValues() {

      // Already entered name
      if (sessionStorage.getItem('elmo_forum_cName')) {
        // Enter commenter name into field
        cNameField.val(sessionStorage.getItem('elmo_forum_cName'));
        cNameField2.val(sessionStorage.getItem('elmo_forum_cName'));
      }
      // Already entered email
      if (sessionStorage.getItem('elmo_forum_cEmail')) {
        // Enter the commenter email into Email field
        cEmailField.val(sessionStorage.getItem('elmo_forum_cEmail'));
      }
      // Already checked remember me
      if (sessionStorage.getItem('elmo_forum_remember') == 'true') {
        // Check the 'Remember me' checkbox
        var rememberVal = ' checked="checked"';
      }

      // Add the remember me checkbox
      var remember  = '<div class="form-type-checkbox form-item checkbox">';
          remember += '<input type="checkbox" name="remember" id="remember" class="form-checkbox"' + rememberVal + ' />';
          remember += '<label for="remember">Remember my name and email address</label>';
          remember += '</div>';
      $(remember).insertBefore(submit);
      $(remember).insertBefore(submit2);  // modal form version
    }


    /**
     * Save form details to sessionStorage
     * or
     * Delete sessionStorage if "remember me" is unchecked
     */
    function saveOrDeleteValues(e) {

      // Check if the "Remember me" checkbox is checked on the submitted form
      var rememberVal = $(this).closest($(cform)).find('#remember').prop('checked');

      // Save values to sessionStorage
      if (rememberVal == true) {
        // modal form version
        var commenterName = $(this).closest($(cform)).find('.form-item-name input').val();
        sessionStorage.setItem('elmo_forum_cName', commenterName);
        sessionStorage.setItem('elmo_forum_cEmail', cEmailField.val());
        sessionStorage.setItem('elmo_forum_remember', rememberVal);
      }

      // Delete sessionStorage values
      else {
        sessionStorage.removeItem('elmo_forum_cName');
        sessionStorage.removeItem('elmo_forum_cEmail');
        sessionStorage.removeItem('elmo_forum_remember');
      }

    };


    /**
     * Simple check for session storage.
     */
    function isSessionStorageAvailable() {

      if (!(JSON.stringify && JSON.parse)) {
        return false;
      }

      try {
        return 'sessionStorage' in window && window.sessionStorage !== null;
      }
      catch (pError) {
        return false;
      }
    }

  });

})(jQuery);
;
(function ($) {

Drupal.googleanalytics = {};

$(document).ready(function() {

  // Attach mousedown, keyup, touchstart events to document only and catch
  // clicks on all elements.
  $(document.body).bind("mousedown keyup touchstart", function(event) {

    // Catch the closest surrounding link of a clicked element.
    $(event.target).closest("a,area").each(function() {

      // Is the clicked URL internal?
      if (Drupal.googleanalytics.isInternal(this.href)) {
        // Skip 'click' tracking, if custom tracking events are bound.
        if ($(this).is('.colorbox')) {
          // Do nothing here. The custom event will handle all tracking.
          //console.info("Click on .colorbox item has been detected.");
        }
        // Is download tracking activated and the file extension configured for download tracking?
        else if (Drupal.settings.googleanalytics.trackDownload && Drupal.googleanalytics.isDownload(this.href)) {
          // Download link clicked.
          ga("send", "event", "Downloads", Drupal.googleanalytics.getDownloadExtension(this.href).toUpperCase(), Drupal.googleanalytics.getPageUrl(this.href));
        }
        else if (Drupal.googleanalytics.isInternalSpecial(this.href)) {
          // Keep the internal URL for Google Analytics website overlay intact.
          ga("send", "pageview", { "page": Drupal.googleanalytics.getPageUrl(this.href) });
        }
      }
      else {
        if (Drupal.settings.googleanalytics.trackMailto && $(this).is("a[href^='mailto:'],area[href^='mailto:']")) {
          // Mailto link clicked.
          ga("send", "event", "Mails", "Click", this.href.substring(7));
        }
        else if (Drupal.settings.googleanalytics.trackOutbound && this.href.match(/^\w+:\/\//i)) {
          if (Drupal.settings.googleanalytics.trackDomainMode != 2 || (Drupal.settings.googleanalytics.trackDomainMode == 2 && !Drupal.googleanalytics.isCrossDomain(this.hostname, Drupal.settings.googleanalytics.trackCrossDomains))) {
            // External link clicked / No top-level cross domain clicked.
            ga("send", "event", "Outbound links", "Click", this.href);
          }
        }
      }
    });
  });

  // Track hash changes as unique pageviews, if this option has been enabled.
  if (Drupal.settings.googleanalytics.trackUrlFragments) {
    window.onhashchange = function() {
      ga('send', 'pageview', location.pathname + location.search + location.hash);
    }
  }

  // Colorbox: This event triggers when the transition has completed and the
  // newly loaded content has been revealed.
  $(document).bind("cbox_complete", function () {
    var href = $.colorbox.element().attr("href");
    if (href) {
      ga("send", "pageview", { "page": Drupal.googleanalytics.getPageUrl(href) });
    }
  });

});

/**
 * Check whether the hostname is part of the cross domains or not.
 *
 * @param string hostname
 *   The hostname of the clicked URL.
 * @param array crossDomains
 *   All cross domain hostnames as JS array.
 *
 * @return boolean
 */
Drupal.googleanalytics.isCrossDomain = function (hostname, crossDomains) {
  /**
   * jQuery < 1.6.3 bug: $.inArray crushes IE6 and Chrome if second argument is
   * `null` or `undefined`, http://bugs.jquery.com/ticket/10076,
   * https://github.com/jquery/jquery/commit/a839af034db2bd934e4d4fa6758a3fed8de74174
   *
   * @todo: Remove/Refactor in D8
   */
  if (!crossDomains) {
    return false;
  }
  else {
    return $.inArray(hostname, crossDomains) > -1 ? true : false;
  }
};

/**
 * Check whether this is a download URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isDownload = function (url) {
  var isDownload = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  return isDownload.test(url);
};

/**
 * Check whether this is an absolute internal URL or not.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternal = function (url) {
  var isInternal = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return isInternal.test(url);
};

/**
 * Check whether this is a special URL or not.
 *
 * URL types:
 *  - gotwo.module /go/* links.
 *
 * @param string url
 *   The web url to check.
 *
 * @return boolean
 */
Drupal.googleanalytics.isInternalSpecial = function (url) {
  var isInternalSpecial = new RegExp("(\/go\/.*)$", "i");
  return isInternalSpecial.test(url);
};

/**
 * Extract the relative internal URL from an absolute internal URL.
 *
 * Examples:
 * - http://mydomain.com/node/1 -> /node/1
 * - http://example.com/foo/bar -> http://example.com/foo/bar
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   Internal website URL
 */
Drupal.googleanalytics.getPageUrl = function (url) {
  var extractInternalUrl = new RegExp("^(https?):\/\/" + window.location.host, "i");
  return url.replace(extractInternalUrl, '');
};

/**
 * Extract the download file extension from the URL.
 *
 * @param string url
 *   The web url to check.
 *
 * @return string
 *   The file extension of the passed url. e.g. "zip", "txt"
 */
Drupal.googleanalytics.getDownloadExtension = function (url) {
  var extractDownloadextension = new RegExp("\\.(" + Drupal.settings.googleanalytics.trackDownloadExtensions + ")([\?#].*)?$", "i");
  var extension = extractDownloadextension.exec(url);
  return (extension === null) ? '' : extension[1];
};

})(jQuery);
;
