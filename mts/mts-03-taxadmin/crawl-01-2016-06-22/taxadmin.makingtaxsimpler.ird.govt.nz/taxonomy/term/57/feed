<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Information collection overview</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-collection-overview</link>
 <description>Information and intelligence capabilities are an important aspect of modernising the tax administration.  The future tax administration will involve:
greater use of pre-populated tax returns;
more automated and streamlined information flows; and
a broader approach to compliance based on smarter use of information and a wider range of interventions.
The Government considers that Inland Revenue’s information collection powers work well.  They are generally consistent with those of revenue agencies in other countries.   However, the ways in which information is stored have changed.  More information is stored in the “cloud” and large electronic datasets have become more commonplace.  Therefore the Government is reviewing whether any updates are needed to ensure Inland Revenue’s information collection powers remain fit for purpose.
Tax returns are a key source of information for Inland Revenue.  The Inland Revenue Acts contain a range of information reporting and return requirements.  Outside of the tax return process, Inland Revenue can require a person to provide any information considered “necessary or relevant” to Inland Revenue’s functions.

The Government considers that, in updating Inland Revenue’s information collection powers, a “necessary or relevant” standard should be retained.  Retaining this standard gives the public continued confidence that Inland Revenue will not use its powers to obtain information that is not needed.     
</description>
 <language>en</language>
<item>
 <title>Do you have any comments on the necessary and relevant requirement that is proposed to be retained?</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-collection-overview/do-you-have-any-comments-necessary-and-relevant</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/57&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information collection overview&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;h3&gt;The need for an information collection standard&lt;/h3&gt;
&lt;p&gt;Inland Revenue has powers to require the provision of information, and to access premises to obtain information.  These powers are subject to the requirement that the information sought is “necessary or relevant” to Inland Revenue’s functions.&lt;/p&gt;
&lt;p&gt;Given the broad nature of Inland Revenue’s information gathering powers, the “necessary or relevant” requirement is an important assurance that Inland Revenue will not use its powers to collect information that is not needed.&lt;/p&gt;
&lt;h3&gt;&lt;strong&gt;Question&lt;/strong&gt;&lt;/h3&gt;
&lt;p&gt;Do you have any comments on the necessary and relevant requirement that is proposed to be retained?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Wed, 04 Nov 2015 01:55:27 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">200 at https://taxadmin.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-collection-overview/do-you-have-any-comments-necessary-and-relevant#comments</comments>
</item>
</channel>
</rss>
