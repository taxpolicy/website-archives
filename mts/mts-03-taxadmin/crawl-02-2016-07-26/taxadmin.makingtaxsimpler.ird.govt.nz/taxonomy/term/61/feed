<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Commercial information</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/commercial-information</link>
 <description>Discussions on information sharing in the wider government tend to focus on personal information.  Alongside personal information, Inland Revenue also holds significant amounts of commercial information, much of it sensitive.  Often personal and non-personal information is inextricably linked.  When the two can be separated, it can reduce the value of the information to the user.
Outside of the Tax Administration Act, information about individuals is subject to the requirements of the Privacy Act.  This provides a clear framework for how personal information is treated and how it can be shared.  No such legislative framework exists for business or commercial information.  It may be that there is less concern about some basic business information such as identifying details and income information and more concern about sensitive information, such as information that could prejudice a business commercially.
</description>
 <language>en</language>
<item>
 <title>What types of commercial information might need to be subject to additional protections?</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/commercial-information/what-types-commercial-information-might-need-be-subject</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/commercial-information-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Commercial information&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Inland Revenue is perhaps unique in the breadth and quantity of information it collects and holds.  This is particularly so in relation to commercial information, much of which can be very sensitive.  For example, taxpayers may provide highly sensitive commercial information in order to obtain an advance ruling on the tax implications of a major transaction.  This could, for example, be an as yet commercially confidential future transaction such as a takeover or restructure, or a complex financing transaction, or an advance pricing agreement.  Equally the audit and disputes processes can result in Inland Revenue obtaining very sensitive commercial information.&lt;/p&gt;
&lt;p&gt;On the other hand, Inland Revenue also holds a wealth of information, including basic data and statistics about non-individual taxpayers that might be of use in the wider government context without prejudicing the commercial position of the taxpayer or taxpayers to whom it relates.&lt;/p&gt;
&lt;p&gt;It is important to find an appropriate dividing line to balance the sharing of data, when that would aid the integrity of the tax system or lead to enhanced cross-government processes, and the need to ensure that businesses are not commercially prejudiced.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 05 Nov 2015 23:17:29 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">207 at https://taxadmin.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/commercial-information/what-types-commercial-information-might-need-be-subject#comments</comments>
</item>
</channel>
</rss>
