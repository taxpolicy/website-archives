<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Information sharing - Overview</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-sharing-overview</link>
 <description>Information sharing between government agencies is generally regulated by the Privacy Act.  In 2013 new rules were put in place to allow “approved information sharing agreements” between agencies delivering public services.  A specific exception to the tax secrecy rule allows Inland Revenue to enter into such agreements.  The agreements relate to “personal information about an identifiable individual”.  Therefore not all taxpayer-specific information held by Inland Revenue can be shared using these agreements.
Inland Revenue currently has information-sharing agreements with 11 agencies.  These are authorised either by specific exceptions to the tax secrecy rule, or information-matching or information sharing rules in the Privacy Act.
International information sharing
Greater information sharing is not only a domestic issue.  Recent years have seen a significant increase in expectations of international sharing of tax information.  Providing information to overseas tax agencies has long been part of international Double Tax Agreements.  More recent initiatives include primarily the American Foreign Account Tax Compliance Act (FATCA) and the OECD-led Automatic Exchange of Information (AEOI).  These new initiatives mean that the magnitude and automated nature of information exchanges between tax agencies will significantly increase.
Cross-agency information sharing in New Zealand
The Government expects agencies to work together to deliver improved services and results for New Zealanders.  For Inland Revenue, strict secrecy rules can act, or be perceived, as a barrier to working more collaboratively with other agencies.  As the Government seeks greater information sharing across agencies, tax secrecy is seen as an increasing barrier.
The Government is currently considering how to better use agencies’ information.  The Data Futures Forum, a working group set up by Ministers provided some recommendations about better use of data, based on guiding principles of value, trust, inclusion and control.  Subsequently the formation of the Data Futures Partnership has been announced, a cross-sector group that will work together to drive high-value and high-trust data use.
The increasing momentum to share more information and the current focus on how to better use government information, makes this an appropriate time to consider Inland Revenue’s secrecy rules.
</description>
 <language>en</language>
</channel>
</rss>
