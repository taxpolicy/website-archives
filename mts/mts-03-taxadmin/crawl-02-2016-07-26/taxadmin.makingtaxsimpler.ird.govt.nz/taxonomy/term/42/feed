<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taxadmin.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Towards a New Tax Administration Act - Tax secrecy or taxpayer confidentiality?</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/tax-secrecy-or-taxpayer-confidentiality</link>
 <description>While the tax secrecy rule increasingly causes tension for Inland Revenue, a starting principle that taxpayer information should be confidential is a longstanding element of New Zealand tax law and is consistent with international practice.  The Government does not therefore propose to step completely away from the concept.
Inland Revenue is perhaps unique in the quantity and breadth of information it holds. It collects and holds information on virtually all New Zealanders, and most corporate and other entities, such as trusts and partnerships.  In some cases the information can be sensitive, particularly in the commercial context.
 The current tax secrecy rule covers all matter relating to the Inland Revenue Acts and is therefore not limited to taxpayer-specific information.  In Australia, Canada and the United States, the information subject to tax secrecy is much narrower, being generally limited to information that would identify (directly or indirectly) the taxpayer to whom it relates.
The Government proposes to narrow the ambit of the secrecy rule so that it only applies to information that would directly or indirectly identify a taxpayer.
</description>
 <language>en</language>
<item>
 <title>Do you agree with narrowing the coverage of secrecy to information that can directly or indirectly identify a taxpayer?</title>
 <link>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/tax-secrecy-or-taxpayer-confidentiality/do-you-agree-narrowing-coverage-secrecy</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/forum/42&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Tax secrecy or taxpayer confidentiality?&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Under this proposal the confidentiality of a taxpayer’s individual affairs would remain, as a starting point, protected.  This would not mean individual information would never be disclosed, but rather, as is the case now, a specific exception authorised by legislation would be required.&lt;/p&gt;
&lt;p&gt;In narrowing the tax secrecy rule, appropriate protections would remain in the Tax Administration Act for sensitive information about Inland Revenue processes, or when the release of information would be damaging to the integrity of the tax system.&lt;/p&gt;
&lt;p&gt;Narrowing the tax secrecy rule would allow Inland Revenue to assist with more requests for information, when that information is anonymised or aggregated.  It would also enable Inland Revenue to provide more information in response to Official Information Act requests.&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Thu, 05 Nov 2015 22:52:34 +0000</pubDate>
 <dc:creator>kelly.thomas</dc:creator>
 <guid isPermaLink="false">204 at https://taxadmin.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taxadmin.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/tax-secrecy-or-taxpayer-confidentiality/do-you-agree-narrowing-coverage-secrecy#comments</comments>
</item>
</channel>
</rss>
