<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://invinfo.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Investment Income Information - More information would be provided, more often</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/more-information-would-be-provided-more-often</link>
 <description>Currently, Inland Revenue doesn’t receive any information about individual taxpayers from companies showing the total dividends received by each taxpayer and the tax withheld from those dividends.  Inland Revenue only receives summary information showing the total dividends and tax paid by the company.
This means Inland Revenue is unable to:
pre-populate this information onto the recipient’s tax records; and
attribute this income to the person for social policy purposes.
More Information would be provided, more often
It is proposed that Inland Revenue receive more frequent and detailed information from companies on the dividends they pay to their shareholders.
The detailed information required from companies would be the:
amount of the dividend or interest paid to each recipient;
recipient’s details (name and address, as well as IRD number (if held) and date of birth if held);
amount of imputation credits; and
amount of RWT credits.
This information would be required in the month following the month in which the dividend was paid to shareholders.  Due dates for filing returns and making payments will remain the same (the 20th of the following month), the only difference is that instead of providing summary information at that time, detailed information would be required.  For example, if a company paid a dividend in June, detailed information about that dividend would need to be provided to Inland Revenue by 20 July.
These additional requirements aren’t expected to impose a significant burden on companies as the information required is already held by companies and provided to their shareholders.
The introduction of the online form would make it easier for small companies to supply this information to Inland Revenue as this information would be able to be entered onto the form, with the ability to save the information for future use.  For companies paying a regular dividend to the same shareholders, providing this information could be as simple as clicking a button.
</description>
 <language>en</language>
<item>
 <title>Would companies have difficulties providing detailed information to Inland Revenue?  If so, how could this be made easier?</title>
 <link>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/more-information-would-be-provided-more-often/would-companies-have-difficulties</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/more-information-would-be-provided-more-often-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;More information would be provided, more often&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Would companies have difficulties providing detailed information to Inland Revenue?  If so, how could this be made easier?&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-this-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Sun, 26 Jun 2016 19:37:32 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">193 at https://invinfo.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://invinfo.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/more-information-would-be-provided-more-often/would-companies-have-difficulties#comments</comments>
</item>
</channel>
</rss>
