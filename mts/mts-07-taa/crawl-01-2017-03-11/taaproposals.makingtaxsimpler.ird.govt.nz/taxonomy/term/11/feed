<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taaproposals.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Proposals for modernising the Tax Administration Act - Overview</title>
 <link>https://taaproposals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/overviewa</link>
 <description>One of the Government’s four priorities is delivering its Better Public Services objectives within tight fiscal constraints.  A key aspect of Better Public Services is improving the use of information within and across agencies.  The Government is focused on achieving better outcomes for New Zealanders through wider and smarter use of data held by government.
Information is critical to Inland Revenue’s ability to perform its functions.  Much of that information is provided by taxpayers.  For taxpayers to be comfortable providing their information they need to feel that the information requested of them is reasonable and is treated appropriately by Inland Revenue.  Currently, surety is given by what is often referred to as the “tax secrecy” rule – which essentially requires that information provided to Inland Revenue is for tax purposes and will only be used for such purposes.
Tax secrecy, or at least the component that relates to the confidentiality of a taxpayer’s individual affairs, is seen as critical to the integrity of the tax system. 
However, the current rule can lead to tensions – in particular tensions between:
confidentiality and wider government objectives, including the more efficient operation of government and the provision of services that can be achieved through increased cross-government information sharing;
confidentiality and the Official Information Act principle of open access to information held by government.
The previous discussion document Making Tax Simpler: Towards a new Tax Administration Act began to explore these issues, resulting in a range of submissions.  In this new consultation the Government is setting out its further thinking and proposals for the future tax confidentiality and disclosure framework, incorporating those submissions.
Greater detail can be found in Chapter 2 of the full discussion document Making Tax Simpler: Proposals for modernising the Tax Administration Act.
</description>
 <language>en</language>
</channel>
</rss>
