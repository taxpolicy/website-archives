<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taaproposals.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Proposals for modernising the Tax Administration Act - Reason for confidentiality</title>
 <link>https://taaproposals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/reason-confidentiality</link>
 <description>For most public sector agencies the primary rules governing collection and disclosure of information are found in the Privacy Act 1993 and the Official Information Act 1982. For Inland Revenue, the primary rules are contained in Part 4 of the Tax Administration Act (“TAA”) which sets out the tax secrecy rule and its exceptions.
The confidentiality of tax information is important for three key reasons.
It is seen as a balance for Inland Revenue’s information-gathering powers. Revenue agencies are traditionally granted wide information-gathering powers so they can ensure that taxpayers are meeting their obligations.
Confidentiality has also traditionally been considered necessary to promote compliance. The rationale is that taxpayers will be more comfortable providing information to Inland Revenue if they are assured it is not shared.
The courts have recently referred to the principle of taxpayer privacy. The right of taxpayers to have their information kept confidential is now recognised in section 6 of the TAA in defining the integrity of the tax system.
A further consideration is changing public expectations. In the international, revenue  context, there is a significant increase in the information being shared, and increasing public expectation that information is available. There is also greater acceptance of information sharing in New Zealand, provided that appropriate safeguards are in place. The Government will be continuing to explore the social licence for data sharing through the work of the Data Futures Partnership. The Partnership is engaging with New Zealanders through a participatory process to inform the development of guidelines designed to support organisations seeking to secure the trust and confidence of the people whose data they are using.
</description>
 <language>en</language>
</channel>
</rss>
