<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://taaproposals.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Proposals for modernising the Tax Administration Act - Information sharing with consent</title>
 <link>https://taaproposals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-sharing-consent</link>
 <description>Under the Privacy Act, individuals may consent to their information being shared, as privacy is theirs to waive. In contrast, confidentiality of tax information is an obligation imposed on Inland Revenue officers and the consent of the person to whom the information relates is no defence to breaching confidentiality.
In Towards a new Tax Administration Act the Government proposed that taxpayers be able to consent to the release of their information to other government agencies. This will allow taxpayers better use of cross-government services, such as updating their contact details more simply across government agencies. A majority of submitters favoured this proposal, provided it was limited to within government. The Government intends to proceed with this proposal.
The Government has recently announced the development of the “data highway” or Data Access Service as a key part of the social investment approach. Exchange of information is based on the informed consent of the individuals concerned. Allowing information to be shared by consent would enable Inland Revenue to become a part of the data exchange. Inland Revenue holds a significant amount of information, and this will be important to the successful functioning of the data exchange. Enabling consent-based sharing will not affect Inland Revenue’s ability to share information without consent if the authority to do so exists.
As these cases will involve taxpayer consent, the Government proposes that Inland Revenue be able to share information with other agencies for the provision of public services if a formal agreement for such sharing is in place. Such agreements would need to specify appropriate conditions for security and handling of information, and include processes to ensure that each taxpayer’s consent is properly obtained and recorded.
As with the Approved Information Sharing Act model in the Privacy Act, it is proposed that sharing be “for the provision of public services” rather than being strictly confined to being within government departments. This will enable, for example, Non-Government Organisations (NGOs) delivering public services on contract to a government department to also have access to the information if appropriate.
 
Example – consent-based sharing for social service provision
A regional NGO assists people to find affordable housing and negotiate their housing-related government entitlements. The NGO has a service agreement with the Ministry of Social Development to provide these services. Customers can be referred by the Ministry or approach the NGO directly.
In order to provide the best service to customers the NGO needs access to up-to-date personalised information. This includes income and other information (for example relating to other social policy entitlements and obligations) held by Inland Revenue. The NGO obtains the informed consent of its customers to access this information.
Inland Revenue, the Ministry of Social Development and the NGO (and potentially other NGOs offering the same service elsewhere) would sign an agreement. Inland Revenue could then provide information to the Ministry and/or the NGO on the customer’s income and any other agreed data points. The agreement would include provision for information security and proof that customers had properly given informed consent. (This type of data exchange might be facilitated by the Data Access Service discussed above.)

</description>
 <language>en</language>
<item>
 <title>What are your thoughts on the proposal to allow consent-based sharing of Inland Revenue information for the provision of public services?</title>
 <link>https://taaproposals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-sharing-consent/what-are-your-thoughts-proposal-allow-consent-based</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/information-sharing-consent-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Information sharing with consent&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;ul class=&quot;links list-inline&quot;&gt;&lt;li class=&quot;help-link first last&quot;&gt;&lt;a href=&quot;/using-website&quot; title=&quot;How to use this site.&quot; class=&quot;btn btn-xs btn-default&quot;&gt;&lt;span class=&quot;help-link glyphicon glyphicon-question-sign&quot;&gt;&lt;/span&gt; help&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
 <pubDate>Wed, 30 Nov 2016 21:17:37 +0000</pubDate>
 <dc:creator>ryan.hamilton</dc:creator>
 <guid isPermaLink="false">173 at https://taaproposals.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://taaproposals.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/information-sharing-consent/what-are-your-thoughts-proposal-allow-consent-based#comments</comments>
</item>
</channel>
</rss>
