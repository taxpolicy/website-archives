<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xml:base="https://vision.makingtaxsimpler.ird.govt.nz"  xmlns:dc="http://purl.org/dc/elements/1.1/">
<channel>
 <title>Green Paper - Business Tax</title>
 <link>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax</link>
 <description>Businesses have already stated that speed and certainty and issues with provisional tax are important concerns.  Businesses want access to the right people at the right time at Inland Revenue to ensure they are doing the right things.
A particular focus of business transformation is to ensure it becomes easier for small businesses to comply with their tax obligations, reducing their compliance costs and improving overall levels of compliance.  It is important that Inland Revenue is more proactive and sophisticated in its approach to providing assistance to small businesses.
Taxes shouldn&#039;t be a minefield for small businesses.  Complying should be easy. 
Currently, businesses are often forced to duplicate processes in order to comply with information requirements.  At the moment, one size fits all for customers; new technology provides an opportunity to rationalise current tax returns for businesses.  The focus of any review should be on providing relevant and timely information efficiently.
 
</description>
 <language>en</language>
<item>
 <title>What are the key tax issues that businesses face?</title>
 <link>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/what-are-key-tax-issues-businesses-face</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/business-tax-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Business Tax&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;The tax system should be easy for businesses to comply with, ensuring they spend more time on running their businesses and less time on tax.  If it&#039;s easy to do the right thing, overall levels of compliance should increase.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;What are the key tax administration issues currently facing businesses?  Are there any particular areas that present concrete ways of increasing speed and certainty?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Are there aspects of running a business that present specific tax administration issues for you – such as the impact of taking on more staff?&lt;/strong&gt;&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Tue, 24 Mar 2015 20:35:43 +0000</pubDate>
 <dc:creator>suzanne.wallace</dc:creator>
 <guid isPermaLink="false">52 at https://vision.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/what-are-key-tax-issues-businesses-face#comments</comments>
</item>
<item>
 <title>How could provisional tax be improved?</title>
 <link>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/how-could-provisional-tax-be-improved</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/business-tax-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Business Tax&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Provisional tax is the mechanism by which most businesses pay tax during the year.  The exact amount of tax to pay for an income year can only be determined after the year has ended.  The provisional tax rules, however, are designed to ensure that tax is paid during the year, rather than at the end. &lt;/p&gt;
&lt;p&gt;For example, the calculation and payment of business income tax could be based more on when income is earned during the year – much like a PAYE for business.  This has the potential to simplify the calculation of provisional tax, create more certainty for businesses, and better reflect cashflow.  Alternatively, a simplified system where provisional tax payments are based on another proxy (for example, a percentage tailored on a business’s turnover) could also be investigated.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;How important is improving the provisional tax rules in reducing compliance costs for business?  Are there other more important issues the Government should be focusing on instead, or as well?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;The Government seeks feedback on ideas for more effective, practical and simple methods of calculating and paying provisional tax. How could provisional tax be better aligned to other business processes?&lt;/strong&gt;&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Tue, 24 Mar 2015 20:34:31 +0000</pubDate>
 <dc:creator>suzanne.wallace</dc:creator>
 <guid isPermaLink="false">51 at https://vision.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/how-could-provisional-tax-be-improved#comments</comments>
</item>
<item>
 <title>Are we heading in the right direction for small businesses?</title>
 <link>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/are-we-heading-right-direction-small-businesses</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/business-tax-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Business Tax&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Tax shouldn&#039;t be a minefield for small businesses.  Complying should be easy.  It may be that there is assistance that Inland Revenue could provide specifically for small businesses.  This could include encouraging the use of improved business systems and accounting software that meets specific standards to help ensure the first few years of a business’s life-cycle are successful.&lt;/p&gt;
&lt;p&gt;Other forms of assistance could involve ensuring that the right support is available at key events that may result in tax obligations, such as taking on new staff for the first time.  Ensuring that businesses get it right first time will be a real focus.  For example, software would have the ability to help users correctly classify transactions to ensure tax obligations are correctly met right from the start.&lt;/p&gt;
&lt;p&gt;Small businesses may also benefit from some tax rules being “simplified”.  This doesn&#039;t mean creating specific tax concessions solely to get a “tax break”.  Rather, the focus will be on investigating some changes that result in tax simplification for small businesses.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Is the proposed direction outlined above the correct focus to provide benefits for small businesses or are there other more important ways of helping small businesses?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;Are there any areas where you think tax for small businesses can be simplified, without creating specific tax breaks?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;A particular focus is to ensure that small businesses achieve higher levels of compliance – what are the most important practical ways of promoting and achieving higher levels of compliance?&lt;/strong&gt;&lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Tue, 24 Mar 2015 20:33:46 +0000</pubDate>
 <dc:creator>suzanne.wallace</dc:creator>
 <guid isPermaLink="false">50 at https://vision.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/are-we-heading-right-direction-small-businesses#comments</comments>
</item>
<item>
 <title>How can we make it easier for businesses to supply information?</title>
 <link>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/how-can-we-make-it-easier-businesses-supply-information</link>
 <description>&lt;div class=&quot;field field-name-taxonomy-forums field-type-taxonomy-term-reference field-label-above&quot;&gt;&lt;div class=&quot;field-label&quot;&gt;Forums:&amp;nbsp;&lt;/div&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; rel=&quot;sioc:has_container&quot;&gt;&lt;a href=&quot;/learn-more-tell-us/business-tax-0&quot; typeof=&quot;sioc:Container sioc:Forum&quot; property=&quot;rdfs:label skos:prefLabel&quot; datatype=&quot;&quot;&gt;Business Tax&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;field field-name-body field-type-text-with-summary field-label-hidden&quot;&gt;&lt;div class=&quot;field-items&quot;&gt;&lt;div class=&quot;field-item even&quot; property=&quot;content:encoded&quot;&gt;&lt;p&gt;Filing paper tax returns is an out-dated concept.  Currently, businesses are often forced to duplicate processes in order to comply with information requirements.  From Inland Revenue’s perspective, the information may not be the type that most effectively allows it to tailor services to customers.&lt;/p&gt;
&lt;p&gt;In the future, new technology may provide an opportunity to rationalise current tax returns for businesses.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;What should be taken into account when looking at the provision of business income information?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt; &lt;/p&gt;
&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;</description>
 <pubDate>Tue, 24 Mar 2015 20:33:06 +0000</pubDate>
 <dc:creator>suzanne.wallace</dc:creator>
 <guid isPermaLink="false">49 at https://vision.makingtaxsimpler.ird.govt.nz</guid>
 <comments>https://vision.makingtaxsimpler.ird.govt.nz/learn-more-tell-us/business-tax/how-can-we-make-it-easier-businesses-supply-information#comments</comments>
</item>
</channel>
</rss>
